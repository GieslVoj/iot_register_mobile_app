package com.giesl.centralregister.classes;

import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.Objects;

public class Device extends RegisterModel
{



    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/devices/1/",
     *         "id": 1,
     *         "name": "zarizeni_1",
     *         "location": "SRID=4326;POINT (14.45480417512606 50.04379728768358)",
     *         "HWID": "HWID1",
     *         "inventoryNumber": "",
     *         "ignoreAlarms": false,
     *         "comment": "",
     *         "fk_DeviceStatus": 1,
     *         "fk_Manufacturer": 1,
     *         "fk_DeviceOld": null,
     *         "types": [
     *             1
     *         ],
     *         "contracts": [
     *             1
     *         ],
     *         "groups": [],
     *         "events": [],
     *         "images": [],
     *         "fk_Company": 1,
     *         "fk_PrimaryDeviceGroup": null,
     *         "color": "#0E23FF",
     *         "enabled": true,
     *         "dateinstalled": "2021-03-02"
     *     },
     *
     */
    protected boolean ignoreAlarms;
    protected String nick, HWID, inventoryNumber;
    protected LatLng location;
    protected ArrayList<DeviceType> types = new ArrayList<>();
    protected DeviceStatus fk_DeviceStatus;
    protected Manufacturer fk_Manufacturer;
    protected Device fk_DeviceOld;
    protected ArrayList<Contract> contracts = new ArrayList<>();
    protected ArrayList<DeviceGroup> groups = new ArrayList<>();
    protected ArrayList<DeviceEvent> events = new ArrayList<>();
    protected ArrayList<Image> images = new ArrayList<>();
    protected Company fk_Company;
    protected int fk_DeviceOld_id;
    protected int color;
    protected DeviceGroup primaryGroup;
    protected int primaryGroupId = -1;
    protected boolean enabled;
    protected Date dateInstalled;

    public Device()
    {

    }

    /**
     * Creates Device without id and location
     * For Device, that will be post to db
     * @param name
     * @param ignoreAlarms
     * @param HWID
     * @param inventoryNumber
     * @param types
     * @param fk_DeviceStatus
     * @param fk_Manufacturer
     * @param fk_DeviceOld
     * @param contracts
     * @param groups
     * @param events
     * @param images
     * @param fk_Company
     */
    public Device(String name, boolean ignoreAlarms, String HWID, String inventoryNumber, ArrayList<DeviceType> types, DeviceStatus fk_DeviceStatus, Manufacturer fk_Manufacturer, Device fk_DeviceOld, ArrayList<Contract> contracts, ArrayList<DeviceGroup> groups, ArrayList<DeviceEvent> events, ArrayList<Image> images, Company fk_Company, int color, int primaryGroupId, boolean enabled)
    {
        this.name = name;
        this.ignoreAlarms = ignoreAlarms;
        this.HWID = HWID;
        this.inventoryNumber = inventoryNumber;
        this.types = types;
        this.fk_DeviceStatus = fk_DeviceStatus;
        this.fk_Manufacturer = fk_Manufacturer;
        this.fk_DeviceOld = fk_DeviceOld;
        this.contracts = contracts;
        this.groups = groups;
        this.events = events;
        this.images = images;
        this.fk_Company = fk_Company;
        this.color = color;
        this.primaryGroupId = primaryGroupId;
        this.enabled = enabled;
    }

    /**
     * Constructor for device, that is loaded from db with all attributes
     * @param id
     * @param name
     * @param company
     * @param location
     * @param hwid
     * @param inventoryNumber
     * @param ignoreAlarms
     * @param comment
     * @param status
     * @param manufacturer
     * @param oldDevice
     * @param images
     * @param types
     */
    public Device(int id, String name,String nick, Company company, LatLng location, String hwid, String inventoryNumber, boolean ignoreAlarms, String comment, DeviceStatus status, Manufacturer manufacturer, int oldDevice, ArrayList<Image> images, ArrayList<DeviceType> types, int color, int primaryGroupId, boolean enabled, Date dateInstalled)
    {
        this.id = id;
        this.name = name;
        this.nick = nick;
        this.HWID = hwid;
        this.inventoryNumber = inventoryNumber;
        this.location = location;
        this.comment = comment;
        this.ignoreAlarms = ignoreAlarms;
        this.fk_DeviceStatus = status;
        this.fk_Manufacturer = manufacturer;
        this.fk_DeviceOld_id = oldDevice;
        this.fk_Company = company;
        this.images = images;
        this.types = types;
        this.color = color;
        this.primaryGroupId = primaryGroupId;
        this.enabled = enabled;
        this.dateInstalled = dateInstalled;
    }


    public void addGroup(DeviceGroup group)
    {
        this.groups.add(group);
    }

    public void addEvent(DeviceEvent event)
    {
        this.events.add(event);
    }

    public void addImage(Image image)
    {
        this.images.add(image);
    }

    public void addContract(Contract contract)
    {
        this.contracts.add(contract);
    }

    public void addType(DeviceType type)
    {
        this.types.add(type);
    }

    public void setOldDevice(Device device)
    {
        this.fk_DeviceOld = device;
    }

    public boolean isIgnoreAlarms()
    {
        return ignoreAlarms;
    }

    public String getHWID()
    {
        return HWID;
    }

    public String getInventoryNumber()
    {
        return inventoryNumber;
    }

    public LatLng getLocation()
    {
        return location;
    }

    public ArrayList<DeviceType> getTypes()
    {
        return types;
    }

    public DeviceStatus getFk_DeviceStatus()
    {
        return fk_DeviceStatus;
    }

    public Manufacturer getFk_Manufacturer()
    {
        return fk_Manufacturer;
    }

    public Device getFk_DeviceOld()
    {
        return fk_DeviceOld;
    }

    public ArrayList<Contract> getContracts()
    {
        return contracts;
    }

    public ArrayList<DeviceGroup> getGroups()
    {
        return groups;
    }

    public ArrayList<DeviceEvent> getEvents()
    {
        return events;
    }

    public ArrayList<Image> getImages()
    {
        return images;
    }

    @Override
    public Company getFk_Company()
    {
        return fk_Company;
    }

    public int getFk_DeviceOld_id()
    {
        return fk_DeviceOld_id;
    }

    public int getColor()
    {
       return this.color;
    }

    public Date getDateInstalled()
    {
        return this.dateInstalled;
    }

    /**
     *
     * @return groups names as list
     */
    public ArrayList<String> getGroupsNames()
    {
        ArrayList<String> names = new ArrayList<>();
        for(DeviceGroup g : this.groups)
        {
            names.add(g.getName());
        }
        return names;
    }

    /**
     *
     * @return Types names as list
     */
    public ArrayList<String> getTypesNames()
    {
        ArrayList<String> names = new ArrayList<>();
        for(DeviceType type : this.types)
        {
            names.add(type.getName());
        }
        return names;
    }

    /**
     * @return Devices Status
     */
    public DeviceStatus getStatus()
    {
        return this.fk_DeviceStatus;
    }

    /**
     *
     * @return contracs names as list
     */
    public ArrayList<String> getContractsNames()
    {
        ArrayList<String> names = new ArrayList<>();
        for(RegisterModel type : this.contracts)
        {
            names.add(type.getName());
        }
        return names;
    }

    /**
     *
     * @return events names as list
     */
    public ArrayList<String> getEventsNames()
    {
        ArrayList<String> names = new ArrayList<>();
        for(RegisterModel type : this.events)
        {
            names.add(type.getName());
        }
        return names;
    }

    /**
     * Set device location
     * @param location LatLng
     */
    public void setLocation(LatLng location)
    {
        this.location = location;
    }

    public void setIgnoreAlarms(boolean ignoreAlarms)
    {
        this.ignoreAlarms = ignoreAlarms;
    }

    public void setInventoryNumber(String inventoryNumber)
    {
        this.inventoryNumber = inventoryNumber;
    }

    public void setTypes(ArrayList<DeviceType> types)
    {
        this.types = types;
    }

    public void setFk_DeviceStatus(DeviceStatus fk_DeviceStatus)
    {
        this.fk_DeviceStatus = fk_DeviceStatus;
    }

    public void setFk_Manufacturer(Manufacturer fk_Manufacturer)
    {
        this.fk_Manufacturer = fk_Manufacturer;
    }

    public void setFk_DeviceOld(Device fk_DeviceOld)
    {
        this.fk_DeviceOld = fk_DeviceOld;
        this.fk_DeviceOld_id = fk_DeviceOld.getId();
    }

    public DeviceGroup getPrimaryGroup()
    {
        return this.primaryGroup;
    }

    public void setContracts(ArrayList<Contract> contracts)
    {
        this.contracts = contracts;
    }

    public void setGroups(ArrayList<DeviceGroup> groups)
    {
        this.groups = groups;
    }

    public void setEvents(ArrayList<DeviceEvent> events)
    {
        this.events = events;
    }

    public void setImages(ArrayList<Image> images)
    {
        this.images = images;
    }

    public void setFk_Company(Company fk_Company)
    {
        this.fk_Company = fk_Company;
    }

    public void setFk_DeviceOld_id(int fk_DeviceOld_id)
    {
        this.fk_DeviceOld_id = fk_DeviceOld_id;
    }

    /**
     * Returns device json
     */
    public JSONObject get_json() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put("name", this.name);
        json.put("nick", this.nick);
        String location = String.valueOf(new Formatter(Locale.US).format("%.12f,%.12f",this.location.longitude,this.location.latitude));
        json.put("location", location);
        json.put("HWID", this.HWID);
        json.put("inventoryNumber",this.inventoryNumber);
        json.put("ignoreAlarms", this.ignoreAlarms);
        json.put("comment", this.comment);
        json.put("fk_DeviceStatus", this.fk_DeviceStatus.getId());
        json.put("fk_Manufacturer",this.fk_Manufacturer.getId());
        json.put("types", arrayToStringJson(this.types));
        json.put("contracts",arrayToStringJson(this.contracts));
        json.put("groups", arrayToStringJson(this.groups));
        json.put("events",arrayToStringJson(this.events));
        json.put("images",arrayToStringJson(this.images));
        json.put("fk_Company",this.fk_Company.getId());
        json.put("dateInstalled", getTimeFormatted());
        json.put("enabled", true);
        if(this.fk_DeviceOld != null)
        {
            json.put("fk_DeviceOld", this.fk_DeviceOld_id);
        }
        if(this.primaryGroup != null)
        {
            json.put("fk_PrimaryDeviceGroup", this.primaryGroup.getId());

        }
        return json;
    }


    private JSONArray arrayToStringJson(ArrayList<? extends RegisterModel> list)
    {
        JSONArray ids = null;
        try {
            ids = new JSONArray(Arrays.toString(getModelIds(list)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ids;
    }

    /**
     *
     * @param list list of registerModel
     * @return ids as string array
     */
    private int[] getModelIds(ArrayList<? extends RegisterModel> list)
    {
        int[] ids = new int[list.size()];
        int idx = 0;
        for(RegisterModel rm : list)
        {
            ids[idx] = rm.getId();
            idx++;
        }
        return ids;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return HWID.equals(device.HWID);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(HWID);
    }

    /**
     * Setter for primary group
     * @param primaryGroup new primary group object
     */
    public void setPrimaryDeviceGroup(DeviceGroup primaryGroup)
    {
        this.primaryGroup = primaryGroup;
        this.primaryGroupId = primaryGroup.id;
    }

    /**
     * Get string to marker title, usage in map
     * @return String
     */
    public String getMarkerTitleString()
    {
        return this.name + this.getTypesNames().toString() + "-" + this.fk_DeviceStatus.name;
    }

    /**
     * Get string to marker snippet, usage in map
     * @return String
     */
    public String getSnippetString()
    {
        String msg = "HWID: " + this.HWID + "\n";
        if(this.primaryGroup != null)
        {
            msg+= "PrimaryGroup: " + this.primaryGroup.name + "\n";

        }
        msg += this.comment;
        return msg;
    }

    /**
     * Get current time in format 2021-02-09
     * @return String - YYYY-MM-DD
     */
    private String getTimeFormatted()
    {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd", Locale.UK);
        String formattedDate = df.format(c);
        return formattedDate;
    }

    /**
     * @return enabled field
     */
    public boolean getEnabled() {
        return this.enabled;
    }
}
