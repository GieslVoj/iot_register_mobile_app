package com.giesl.centralregister.classes;

import com.giesl.centralregister.constants.Constants;
import com.google.android.gms.maps.model.LatLng;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.Date;

public class Sensor
{
    private String name,comment = "";
    private int ID,contractID,typeID;
    private SensorType type;
    private LatLng position;
    private Date dateAdded;


    public Sensor(String name, int contractID, int typeID,LatLng position,Date dateAdded,String comment)
    {
        this.name           = name;
        this.contractID     = contractID;
        this.typeID         = typeID;
        this.position       = position;
        this.dateAdded      = dateAdded;
        this.ID             = -1;
        this.comment        = comment;

        //TODO - odebrat
        this.type = new SensorType(0,"test","cyan","car");
        this.type.setIcon(MaterialDrawableBuilder.IconValue.PARKING);
    }

    public Sensor(String name, int contractID, int typeID,LatLng position,Date dateAdded)
    {
        this.name           = name;
        this.contractID     = contractID;
        this.typeID         = typeID;
        this.position       = position;
        this.dateAdded      = dateAdded;
        this.ID             = -1;
    }

    //Getters and setters
    public String getName()
    {
        return name;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int id){ this.ID = id;};

    public int getContractID()
    {
        return contractID;
    }

    public int getTypeID()
    {
        return typeID;
    }

    public SensorType getType()
    {
        return type;
    }

    public LatLng getPosition()
    {
        return position;
    }

    public Date getDateAdded()
    {
        return dateAdded;
    }

    public void setTypeID(int typeID)
    {
        this.typeID = typeID;
    }

    public void setType(SensorType type)
    {
        this.type = type;
    }

    public void setPosition(LatLng position)
    {
        this.position = position;
    }

    public String getComment(){return this.comment;};

    public void setComment(String comment) {this.comment = comment;};

}
