package com.giesl.centralregister.classes;

import com.giesl.centralregister.interfaces.Model;

import java.util.Objects;

public class DeviceType extends RegisterModel implements Model
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/types/1/",
     *         "id": 1,
     *         "name": "typ_1",
     *         "icon": "account_balance",
     *         "fk_Domain": 1,
     *         "fk_Company": 1
     *     },
     *
     */
    private int id;
    private String name;
    private String icon;
    private Domain fk_Domain;
    private Company fk_Company;

    public DeviceType(int id, String name, String icon, Domain domain)
    {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.fk_Domain = domain;

    }

    @Override
    public int getId()
    {
        return id;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public String getIcon()
    {
        return icon;
    }

    public Domain getFk_Domain()
    {
        return fk_Domain;
    }

    @Override
    public Company getFk_Company()
    {
        return fk_Company;
    }

    public int getColor()
    {
        return this.fk_Domain.getColor();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceType that = (DeviceType) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
