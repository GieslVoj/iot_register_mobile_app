package com.giesl.centralregister.classes;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;

public class Supplier extends RegisterModel
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/suppliers/1/",
     *         "id": 1,
     *         "name": "dodavatel 1",
     *         "ICO": null,
     *         "DIC": null,
     *         "location": "SRID=4326;POINT (17.66808185778807 49.22555327139667)",
     *         "dateCreated": "2020-11-04",
     *         "comment": "",
     *         "fk_Company": 3
     *     }
     *
     */
    private String ICO, DIC;
    private LatLng location;
    private Date dateCreated;

    public Supplier(int id, String name, String ico, String dic, LatLng location, String comment, Company company, Date dateCreated)
    {
        this.id = id;
        this.name = name;
        this.ICO = ico;
        this.DIC = dic;
        this.location = location;
        this.comment = comment;
        this.fk_Company = company;
        this.dateCreated = dateCreated;
    }
}
