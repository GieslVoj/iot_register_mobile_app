package com.giesl.centralregister.classes;

public class Role extends RegisterModel
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/roles/1/",
     *         "id": 1,
     *         "name": "role_1",
     *         "fk_Company": 1
     *     },
     *
     */

    public Role()
    {

    }

    public Role(int id, String name, Company company)
    {
        this.id = id;
        this.name = name;
        this.fk_Company = company;
    }
}
