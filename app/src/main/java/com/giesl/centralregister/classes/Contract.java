package com.giesl.centralregister.classes;

import java.util.ArrayList;

public class Contract extends RegisterModel
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/contracts/1/",
     *         "id": 1,
     *         "name": "Kontrakt 1",
     *         "comment": null,
     *         "fk_Company": 1,
     *         "fk_Suppliers": [],
     *         "fk_Devices": [
     *             1,
     *             2
     *         ]
     *     }
     *
     */
    private ArrayList<Supplier> fk_Suppliers = new ArrayList<>();
    private ArrayList<Device> fk_Devices = new ArrayList<>();

    public Contract()
    {

    }

    public Contract(int id, String name, Company company, ArrayList<Supplier> suppliers, ArrayList<Device> devices)
    {
        this.id = id;
        this.name = name;
        this.fk_Company = company;
        this.fk_Suppliers = suppliers;
        this.fk_Devices = devices;
        addFkToDevices();

    }

    private void addFkToDevices()
    {
        for(Device d : this.fk_Devices) d.addContract(this);
    }
}
