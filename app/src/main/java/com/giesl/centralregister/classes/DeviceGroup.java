package com.giesl.centralregister.classes;

import java.util.ArrayList;
import java.util.Objects;

public class DeviceGroup extends RegisterModel
{

    private final int color;
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/groups/9/",
     *         "id": 9,
     *         "name": "parking-149-86",
     *         "comment": "",
     *         "fk_Devices": [],
     *         "color" : #FFFFFF
     *         "fk_Company": 3
     *     },
     *
     */


    public DeviceGroup(int id, String name, String comment,int color,Company company, ArrayList<Device> devices)
    {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.fk_Company = company;
        this.fk_Devices = devices;
        this.color = color;
        addFkToDevices();

    }

    private void addFkToDevices()
    {
        for(Device d : this.fk_Devices)
        {
            d.addGroup(this);
        }
    }


    private ArrayList<Device> getDevices()
    {
        return this.fk_Devices;
    }

    private boolean containsDevice(Device d)
    {
        if(this.fk_Devices.contains(d)) return true;
        return false;
    }

    /**
     * Get device by name, if not in group return null
     * @param name
     * @return
     */
    public Device getDevice(String name)
    {
        for(Device device : this.fk_Devices)
        {
            if(device.getName().equals(name)) return device;
        }
        return null;
    }

    /**
     * Get device from group by id, if device with that id not in group return null
     * @param id
     * @return
     */
    public Device getDevice(int id)
    {
        for(Device device : this.fk_Devices)
        {
            if(device.getId() == id) return device;
        }
        return null;
    }


    /**
     * Get device from group by hwid, if device with that hwid not in group return null
     * @param hwid
     * @return
     */
    public Device getDeviceByHWID(String hwid)
    {
        for(Device device : this.fk_Devices)
        {
            if(device.getHWID().equals(hwid)) return device;
        }
        return null;
    }

    /**
     * get group color
     * @return int
     */
    public int getColor()
    {
        return this.color;
    }

    /**
     * Get all related devices ids
     * @return ArrayList<Integer>
     */
    public ArrayList<Integer> getDevicesIds()
    {
        ArrayList<Integer> ids = new ArrayList<>();
        for(Device d : this.fk_Devices)
        {
            ids.add(d.id);
        }
        return ids;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceGroup that = (DeviceGroup) o;
        return (this.id == that.id);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
