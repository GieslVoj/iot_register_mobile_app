package com.giesl.centralregister.classes;

import java.util.ArrayList;
import java.util.Date;

public class DeviceEvent extends RegisterModel
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/events/11/",
     *         "id": 11,
     *         "name": "testing",
     *         "dateStart": "2020-11-04",
     *         "dateEnd": "2020-11-05",
     *         "comment": "commentary",
     *         "fk_Devices": [
     *             406,
     *             407,
     *             408,
     *             409,
     *             410,
     *             411
     *         ],
     *         "enabled": true,
     *         "fk_Company": 3
     *     }
     *
     */
    private String name,comment;
    private Date dateStart, dateEnd;
    private ArrayList<Device> devices;
    private boolean enabled;
    private Company fk_Company;

    /**
     * Base constructor
     */
    public DeviceEvent()
    {

    }

    /**
     * Constructor for DeviceEvents, with all attributes -> only for loading from db
     * @param id id of object
     * @param name name
     * @param dateStart date when started
     * @param dateEnd date when ended
     * @param comment commentary for object
     * @param devices list of devices
     * @param enabled is event enabled
     * @param company Foreign key to company
     */
    public DeviceEvent(int id, String name, Date dateStart, Date dateEnd, String comment, ArrayList<Device> devices, boolean enabled, Company company)
    {
        this.id = id;
        this.name = name;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.devices = devices;
        this.enabled = enabled;
        this.comment = comment;
        this.fk_Company = company;
        addFkToDevice();
    }

    /**
     * Internal method for adding self to all devices fk_Events
     */
    private void addFkToDevice()
    {
        for(Device d : this.devices) d.addEvent(this);
    }

    /**
     * Return event name
     */
    public String getName()
    {
        return this.name;
    }
}
