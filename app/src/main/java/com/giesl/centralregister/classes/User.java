package com.giesl.centralregister.classes;

import java.util.Map;

public class User {
    private String email;
    private int selectedCompanyID;
    boolean isStaff, isActive;
    Map<Integer, String> companies;

    public User(String email, int selectedCompanyID, boolean isStaff, boolean isActive, Map<Integer, String> companies) {
        this.email = email;
        this.selectedCompanyID = selectedCompanyID;
        this.isStaff = isStaff;
        this.isActive = isActive;
        this.companies = companies;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSelectedCompanyID() {
        return selectedCompanyID;
    }

    public void setSelectedCompanyID(int selectedCompanyID) {
        this.selectedCompanyID = selectedCompanyID;
    }

    public boolean isStaff() {
        return isStaff;
    }

    public void setStaff(boolean staff) {
        isStaff = staff;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Map<Integer, String> getCompanies() {
        return companies;
    }

    public void setCompanies(Map<Integer, String> companies) {
        this.companies = companies;
    }
}
