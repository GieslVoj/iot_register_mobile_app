package com.giesl.centralregister.classes;

import android.graphics.Color;

public class Domain extends RegisterModel
{
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/domains/1/",
     *         "id": 1,
     *         "name": "Domena 1",
     *         "color": "#7AF1FF",
     *         "fk_Company": 1
     *     },
     *
     */
    private int color;

    public Domain(int id, String name, int color)
    {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public Domain()
    {

    }

    public int getColor()
    {
        return this.color;
    }
}
