package com.giesl.centralregister.classes;

import com.giesl.centralregister.constants.Constants;

public class Image extends RegisterModel
{
    private final String url;
    /**
     *
     * {
     *         "url": "http://192.168.43.3:8000/api/images/1/",
     *         "id": 1,
     *         "image": "http://192.168.43.3:8000/media/iot_register/images/FIT-VUT-logo-fs8.png",
     *         "thumbnail": "http://192.168.43.3:8000/media/iot_register/thumbnails/FIT-VUT-logo-fs8_thumb.png"
     *     }
     *
     */

    //Only need id, getting images with custom views

    protected String getUrl;

    public Image(int id, String url)
    {
        this.id = id;
        this.getUrl = Constants.BASE_URL + "/api-image/?pk=" + id;
        this.url = url;

    }

    /**
     * Get image url
     * @return String url
     */
    public String getGetUrl()
    {
        return this.getUrl;
    }

    public String getUrl() {
        return url;
    }
}
