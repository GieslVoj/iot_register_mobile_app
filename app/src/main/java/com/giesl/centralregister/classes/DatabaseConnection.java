package com.giesl.centralregister.classes;


import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.services.cognitoidentityprovider.model.DeviceType;
import com.giesl.centralregister.R;
import com.giesl.centralregister.constants.Constants;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class DatabaseConnection
{



    private static String TAG = "DATABASE CONNECTION";


    //Request methods
    private static final String METHOD_GET      = "GET";
    private static final String METHOD_POST     = "POST";
    private static final String METHOD_DELETE   = "DELETE";
    private static final String METHOD_UPDATE   = "UPDATE";
    private static final String METHOD_PATCH    = "PATCH";

    //frequently used queries templates

    private final String EQUALS = "=eq.";
    private final String IS_TRUE     = "=is.true";
    private final String IS_FALSE    = "=is.false";
    private final String DEVICE_ID_EQUALS   = Constants.TABLE_DEVICE +  Constants.COL_DEVICE_ID +  EQUALS;
    private final String DEVICE_NAME_EQUALS = Constants.TABLE_DEVICE + Constants.COL_DEVICE_NAME + EQUALS;
    private final String TYPE_ID_EQUALS     = Constants.TABLE_TYPE + Constants.COL_TYPE_ID + EQUALS;
    private final String COMPANY_ID_EQUALS  = Constants.TABLE_COMPANY + Constants.COL_COMPANY_ID + EQUALS;
    private final String CONTRACT_ID_EQUALS = Constants.TABLE_CONTRACT + Constants.COL_CONTRACT_ID + EQUALS;

    //QUERIES
    //private final String QUERY_GET_SENSORS = "/Device?enabled=is.true&idContract_Contract=eq.1";
    private final String QUERY_GET_SENSORS_BY_CONTRACT  = Constants.TABLE_DEVICE + Constants.COL_DEVICE_ENABLED + IS_TRUE + Constants.COL_DEVICE_CONTRACTID + EQUALS;
    private final String QUERY_GET_SENSORS_TYPES        = Constants.TABLE_TYPE;
    private final String QUERY_GET_SUPPLIERS            = Constants.TABLE_SUPPLIER;
    private final String QUERY_GET_CONTRACTS            = Constants.TABLE_CONTRACT + Constants.COL_CONTRACT_COMPANYID + EQUALS;
    private final String QUERY_GET_COMPANIES            = Constants.TABLE_COMPANY;

    //Arrays of database values
    private ArrayList<Contract> contracts   = new ArrayList<>();
    private ArrayList<Company> companies    = new ArrayList<>();
    private ArrayList<Sensor> sensors       = new ArrayList<>();
    private ArrayList<SensorType> types     = new ArrayList<>();
    private ArrayList<Supplier> suppliers   = new ArrayList<>();


    public DatabaseConnection()
    {
    }


    private static boolean isReturnCodeOK(int code)
    {
        if(code >= 200 && code <= 300) return true;
        return false;
    }

    /**
     *
     * @return true if network is available
     */
    public static boolean isNetworkAvailable()
    {
        try {
            return new CheckConnection().execute().get();
        } catch (ExecutionException | InterruptedException e) {
            return false;
        }
    }

    /**
     * Send query to db
     * @param query query
     * @return
     */
    private Response sendQuery(Query query)
    {
        try {
            String normalizedQuery = Normalizer.normalize(query.query, Normalizer.Form.NFD);
            normalizedQuery = normalizedQuery.replaceAll("[^\\p{ASCII}]", "");
            query.query = normalizedQuery;
            Response response = new SendData().execute(query).get();
            return response;
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG,e.getMessage());
            return null;
        }
    }
    private Response select(String table, List<String> columns)
    {
        String query = "/" + table + "?select=" + columns.get(0);
        for(String col : columns.subList(1,columns.size()))
        {
            query += "," + col;
        }
        Query q = new Query();
        q.query = query;
        Response response = this.sendQuery(q);
        return response;
    }
    private Response selectEquals(String table, List<String> columns,String columnToCompare, String equals)
    {
        String query = "/" + table + "?select=" + columns.get(0);
        for(String col : columns.subList(1,columns.size()))
        {
            query += "," + col;
        }
        query += "&" + columnToCompare + "=eq." + equals;
        Query q = new Query();
        q.query = query;
        Response response = this.sendQuery(q);
        return response;
    }
    private Response select(String table, String column)
    {
        String query = "/" + table + "?select=" + column;
        Query q = new Query();
        q.query = query;
        Response response = this.sendQuery(q);
        return response;
    }
    private Response selectEquals(String table, String column, String columnToCompare, String equals)
    {
        String query = "/" + table + "?select=" + column + "&" + columnToCompare + "=eq." + equals;
        Query q = new Query();
        q.query = query;
        Response response = this.sendQuery(q);
        return response;
    }
    private Response selectEquals(String table, String columnToCompare, String equals)
    {
        String query = table + columnToCompare + "=eq." + equals;
        Query q = new Query();
        q.query = query;
        q.method = METHOD_GET;
        Response response = this.sendQuery(q);
        return response;
    }

    //TODO
    private String getErrorCodeMessage(int code)
    {
        String msg = "";



        msg += "\nMore info in http://postgrest.org/en/v6.0/api.html HTTP Status Codes";

        return msg;
    }

    /**
     * Adds new sensor to database, check if exists and add it to db
     * @param sensor sensor to add
     * @return response from server
     */
    private Response addSensor(Sensor sensor)
    {

        if(sensorExists(sensor)) return null;
        JSONObject postData = new JSONObject();
        try {
            postData.put(Constants.COL_DEVICE_NAME,sensor.getName());
            postData.put(Constants.COL_DEVICE_LAT,sensor.getPosition().latitude);
            postData.put(Constants.COL_DEVICE_LNG,sensor.getPosition().longitude);
            postData.put(Constants.COL_DEVICE_ENABLED,"true");
            postData.put(Constants.COL_DEVICE_COMMENT,sensor.getComment());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

            Query q = new Query();
            q.query = Constants.TABLE_DEVICE;
            q.postData = postData.toString();
            q.method = METHOD_POST;
            Response response = sendQuery(q);
            try {
                Query q2 = new Query();
                String query = Constants.TABLE_DEVICE + "select=" + Constants.COL_DEVICE_ID + "&" + Constants.COL_CONTRACT_ID + EQUALS + sensor.getContractID() + "&"
                        + Constants.COL_DEVICE_NAME + EQUALS + sensor.getName();
                q2.query = query;
                int newId = Integer.valueOf(sendQuery(q2).response);
                Log.w("ADDED SENSOR","ADDED SENSOR WITH ID: " + newId);
                sensor.setID(newId);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
            this.sensors.add(sensor);
            return response;


    }

    public boolean addNewSensor(Sensor sensor)
    {
        Response r = this.addSensor(sensor);
        if(r == null) return false;
        return isReturnCodeOK(r.returnCode);

    }

    /**
     * Get id of sensor type by name
     * @param name name of type
     * @return int - ID
     */
    public int getDeviceTypeID(String name)
    {
        for(SensorType type : types)
        {
            if (type.name.equals(name))
            {
                return type.ID;
            }
        }
        return -1;
    }

    /**
     * Set sensor parameter enable to false;
     * @param sensor
     * @return response from server
     */
    private Response disableSensor(Sensor sensor)
    {
        //checks if sensor has valid id
        if(sensor.getID() == -1) return null;

        JSONObject data = new JSONObject();
        try {
            data.put(Constants.COL_DEVICE_ENABLED,"false");
            String query = DEVICE_ID_EQUALS + sensor.getID();
            Query q = new Query();
            q.query = query;
            q.method = METHOD_PATCH;
            Response response = sendQuery(q);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Set sensor parameter enable to true
     * @param sensor
     * @return response from server
     */
    private Response enableSensor(Sensor sensor)
    {
        //checks if sensor has valid id
        if(sensor.getID() == -1) return null;

        JSONObject data = new JSONObject();
        try {
            data.put(Constants.COL_DEVICE_ENABLED,"true");

            String query = DEVICE_ID_EQUALS + sensor.getID();
            Query q = new Query();
            q.query = query;
            q.method = METHOD_PATCH;
            Response response = sendQuery(q);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Check if sensor exists
     * @param sensor sensor to find
     * @return true if exists
     */
    private boolean sensorExists(Sensor sensor)
    {
        if(this.findSensor(sensor) != null) return false;
        String response = this.selectEquals(Constants.TABLE_DEVICE,Constants.COL_DEVICE_NAME,sensor.getName()).response;
        if(response.equals("[]") || response.equals("[]\n")) return false;
        return true;
    }

    /**
     * Class for send data to REST APT and gets response code from db
     */
    private static class SendData extends AsyncTask<Query, Void, Response> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        /**
         * params:
         *  [0] URL
         *  [1] data
         *  optional: [2] request method - default POST
         * @return response code
         */
        protected Response doInBackground(Query... queries) {

            HttpURLConnection httpURLConnection = null;
            BufferedReader reader = null;
            Response respose = new Response();

            try {

                Query query = (Query) queries[0];
                URL url = new URL(Constants.RESTAPIADDRESS + query.query);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod(query.method);
                httpURLConnection.setRequestProperty("Authorization", "Bearer " + Constants.API_TOKEN);
                //httpURLConnection.setDoOutput(true);
                if(query.postData != "") {
                    httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                    DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                    wr.writeBytes(query.postData);
                    wr.flush();
                    wr.close();
                }
                httpURLConnection.connect();

                InputStream stream = httpURLConnection.getInputStream();
                respose.returnCode = httpURLConnection.getResponseCode();


                    if(isReturnCodeOK(respose.returnCode)) {
                        reader = new BufferedReader(new InputStreamReader(stream));
                        StringBuffer buffer = new StringBuffer();
                        String line = "";
                        while ((line = reader.readLine()) != null) {
                            buffer.append(line);
                            Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                        }
                        respose.response = buffer.toString();


                    Log.e(TAG,"Bad return code: " + httpURLConnection.getResponseCode());
                    return respose;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return respose;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(new Response());
            Log.e("Receive data", result); // this is expecting a response code to be sent from your server upon receiving the POST data

        }
    }


    private static class CheckConnection extends AsyncTask<Void,Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... voids)
        {
            try {
                InetAddress address = InetAddress.getByName("81.200.53.78");
                return address.isReachable(1000);
            } catch (IOException e) {
                //no internet connection
                return false;
            }
        }
    }

    private ArrayList<Contract> loadContracts(int companyID)
    {
        ArrayList<Contract> contracts = new ArrayList<>();
        Query q = new Query();
        q.query = QUERY_GET_CONTRACTS;
        Response response = sendQuery(q);
        try {
            JSONArray arr = new JSONArray(response.response);
            for(int i = 0;i<arr.length();i++)
            {
                JSONObject jo = arr.getJSONObject(i);
                contracts.add(jsonToContract(jo));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return contracts;
    }

    private ArrayList<SensorType> loadSensorTypes()
    {
        ArrayList<SensorType> types = new ArrayList<>();
        Query q = new Query();
        q.query = QUERY_GET_SENSORS_TYPES;
        Response response = this.sendQuery(q);
        try {
            JSONArray arr = new JSONArray(response.response);
            for(int i = 0; i < arr.length();i++)
            {
                JSONObject jo = arr.getJSONObject(i);
                types.add(jsonToSensorType(jo));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG,"error with loading sensors from db");
        }
        return types;
    }

    private ArrayList<Sensor> loadSensorsByContract(int contractID)
    {
        ArrayList<Sensor> sensors = new ArrayList<>();
        Query q = new Query();
        q.query = QUERY_GET_SENSORS_BY_CONTRACT + contractID;
        Response response = this.sendQuery(q);
        String result = response.response;
        JSONArray arr;
        try {
            arr = new JSONArray(result);
            for(int i = 0;i<arr.length();i++)
            {
                JSONObject js = arr.getJSONObject(i);
                Sensor s = jsonToSensor(js);
                this.sensors.add(s);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return sensors;
    }

    private ArrayList<Company> loadCompanies()
    {
        ArrayList<Company> companies = new ArrayList<>();
        Query q = new Query();
        q.query = QUERY_GET_COMPANIES;
        Response response = this.sendQuery(q);
        try {
            JSONArray arr = new JSONArray(response.response);
            for(int i = 0; i < arr.length();i++)
            {
                JSONObject jo = arr.getJSONObject(i);
                companies.add(jsonToCompany(jo));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG,"error with loading sensors from db");
        }
        return companies;
    }

    public ArrayList<Contract> getContracts()
    {
        return this.contracts;
    }



    public ArrayList<SensorType> getSensorTypes()
    {
        return this.types;
    }

    public ArrayList<Company> getCompanies()
    {
        return this.companies;
    }

    private Sensor jsonToSensor(JSONObject json)
    {
        Sensor sensor = null;
        try {
            int id          = Integer.valueOf(json.getString(Constants.COL_DEVICE_ID));
            String name     = json.getString(Constants.COL_DEVICE_NAME);
            Double lat      = Double.valueOf(json.getString(Constants.COL_DEVICE_LAT));
            Double lng      = Double.valueOf(json.getString(Constants.COL_DEVICE_LNG));
            //TODO set type to Date
            String created  = json.getString(Constants.COL_DEVICE_CREATED);
            String enabled  = json.getString(Constants.COL_DEVICE_ENABLED);
            String comment  = json.getString(Constants.COL_DEVICE_COMMENT);
            int typeID      = Integer.valueOf(json.getString(Constants.COL_DEVICE_TYPEID));
            int contractID = Integer.valueOf(json.getString(Constants.COL_DEVICE_CONTRACTID));
            Date added = new Date();
            sensor = new Sensor(name,contractID,typeID,new LatLng(lat,lng),added,comment);
            sensor.setID(id);


            return sensor;

        }
        catch (Exception e)
        {

        }
        return sensor;
    }

    private SensorType jsonToSensorType(JSONObject json)
    {
        SensorType type = null;

        try {
            int typeid = json.getInt(Constants.COL_TYPE_ID);
            String name = json.getString(Constants.COL_TYPE_NAME);
            String icon = json.getString(Constants.COL_TYPE_ICON);
            String domain = json.getString(Constants.COL_TYPE_DOMAIN);
            String color = json.getString(Constants.COL_TYPE_COLOR);
            type = new SensorType(typeid,name,color,domain);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,"error with parsing sensor type");
            return null;
        }
        return type;
    }

    private Company jsonToCompany(JSONObject json)
    {
        Company company = null;
        try
        {
            int companyID = json.getInt(Constants.COL_COMPANY_ID);
            String name = json.getString(Constants.COL_COMPANY_NAME);
            String email = json.getString(Constants.COL_COMPANY_EMAIL);
            //company = new Company(companyID,name,email);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,"Error with parsing company");
        }

        return company;
    }

    private Contract jsonToContract(JSONObject json)
    {
        Contract contract = null;
        try {
            int contractID = json.getInt(Constants.COL_CONTRACT_ID);
            String name = json.getString(Constants.COL_CONTRACT_NAME);
            String token = json.getString(Constants.COL_CONTRACT_TOKEN);
            String email = json.getString(Constants.COL_CONTRACT_EMAIL);
            String comment = json.getString(Constants.COL_CONTRACT_COMMENT);
            int companyID = json.getInt(Constants.COL_CONTRACT_COMPANYID);
            //contract = new Contract(contractID,name,email,token,companyID,comment);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,"Error with parsing contract");
        }

        return contract;
    }

    private Sensor findSensor(Sensor sensor)
    {
        for(Sensor s : this.sensors)
        {
            if(s.getName().equals(sensor.getName()) && s.getContractID() == sensor.getContractID())
            {
                return s;
            }
        }
        return null;
    }

    private class Query
    {
        public String query;
        public String method = METHOD_GET;
        public String postData = "";

        public Query()
        {

        }
    }

    private static class Response
    {
        public String response;
        public int returnCode = -1;

        public Response()
        {
        }
    }
}
