package com.giesl.centralregister.classes;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Serializer {


    private Iterable<? extends Company> companies;
    private Iterable<? extends Role> roles;
    private Iterable<? extends Domain> domains;
    private Iterable<? extends Supplier> suppliers;
    private Iterable<? extends Image> images;
    private Iterable<? extends Device> devices;
    private Iterable<? extends Contract> contracts;
    private Iterable<? extends DeviceStatus> statuses;
    private Iterable<? extends Manufacturer> manufacturers;
    private Iterable<? extends DeviceGroup> groups;
    private Iterable<? extends DeviceType> types;
    private Iterable<? extends DeviceEvent> events;
    private Iterable<? extends Contact> contacts;

    public Serializer(Iterable<? extends Company> companies, Iterable<? extends Role> roles, Iterable<? extends Domain> domains, Iterable<? extends Supplier> suppliers, Iterable<? extends Image> images, Iterable<? extends Device> devices, Iterable<? extends Contract> contracts, Iterable<? extends DeviceStatus> statuses, Iterable<? extends Manufacturer> manufacturers, Iterable<? extends DeviceGroup> groups, Iterable<? extends DeviceType> types, Iterable<? extends DeviceEvent> events, Iterable<? extends Contact> contacts) {
        this.companies = companies;
        this.roles = roles;
        this.domains = domains;
        this.suppliers = suppliers;
        this.images = images;
        this.devices = devices;
        this.contracts = contracts;
        this.statuses = statuses;
        this.manufacturers = manufacturers;
        this.groups = groups;
        this.types = types;
        this.events = events;
        this.contacts = contacts;
    }

    /*****************************************SERIALIZERS*****************************************/

    /**
     * Serialize json to Manufacturers array
     * @param json_manufacturer
     * @return
     */
    public ArrayList<Manufacturer> serializeManufacturers(JSONArray json_manufacturer) throws JSONException
    {
        ArrayList<Manufacturer> manufacturers = new ArrayList<>();
        for(int i =0;i<json_manufacturer.length();i++)
        {
            JSONObject jo = json_manufacturer.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String ICO = jo.getString("ICO");
            String DIC = jo.getString("DIC");
            LatLng location = parseLocation(jo.getString("location"));
            String comment = jo.getString("comment");
            //Company company = getCompany(jo.getInt("fk_Company"));
            manufacturers.add(new Manufacturer(id, name, ICO, DIC, location, comment));
        }
        this.manufacturers = manufacturers;
        return manufacturers;
    }

    /**
     * Serialize json to array of Suppliers
     * @param json_supplier
     * @return
     */
    public ArrayList<Supplier> serializeSuppliers(JSONArray json_supplier) throws JSONException
    {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        for (int i = 0;i<json_supplier.length();i++)
        {
            JSONObject jo = json_supplier.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String ICO = jo.getString("ICO");
            String DIC = jo.getString("DIC");
            LatLng location = parseLocation(jo.getString("location"));
            String comment = jo.getString("comment");
            Company company = getCompany(jo.getInt("fk_Company"));
            Date dateCreated = parseDate(jo.getString("dateCreated"));
            suppliers.add(new Supplier(id, name, ICO, DIC, location, comment, company, dateCreated));
        }
        this.suppliers = suppliers;
        return suppliers;
    }

    /**
     * Serialize json to array of Events
     * @param json_events
     * @return
     */
    public ArrayList<DeviceEvent> serializeEvents(JSONArray json_events) throws JSONException
    {
        ArrayList<DeviceEvent> events = new ArrayList<>();
        for (int i = 0;i < json_events.length();i++)
        {
            JSONObject jo = json_events.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            Date dateStart = parseDate(jo.getString("dateStart"));
            Date dateEnd = parseDate(jo.getString("dateEnd"));
            String comment = jo.getString("comment");
            ArrayList<Device> devices = getDevices(jo.getJSONArray("fk_Devices"));
            boolean enabled = jo.getBoolean("enabled");
            Company company = getCompany(jo.getInt("fk_Company"));

            events.add(new DeviceEvent(id, name, dateStart, dateEnd,comment, devices, enabled,company));


        }
        this.events = events;
        return events;
    }

    /**
     * Serialize Company json to Company java instances
     * @param json_companies
     * @return
     */
    public ArrayList<Company> serializeCompanies(JSONArray json_companies) throws JSONException
    {
        ArrayList<Company> companies = new ArrayList<>();
        for (int i = 0; i < json_companies.length(); i++) {
            JSONObject jo = json_companies.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String ICO = jo.getString("ICO");
            String DIC = jo.getString("DIC");
            LatLng location = parseLocation(jo.getString("location"));
            Date dateCreated = parseDate(jo.getString("dateCreated"));

            Company company = new Company(id, name, ICO, DIC, location, dateCreated);
            companies.add(company);

        }
        this.companies = companies;
        return companies;
    }

    /**
     * Serialize json array to array of images
     * @param json_images
     * @return
     */
    public ArrayList<Image> serializeImages(JSONArray json_images) throws JSONException
    {
        ArrayList<Image> images = new ArrayList<>();
        for(int i = 0;i<json_images.length();i++)
        {
            JSONObject jo = json_images.getJSONObject(i);
            int id = jo.getInt("id");
            String url = jo.getString("url");
            images.add(new Image(id, url));

        }
        this.images = images;
        return images;
    }

    /**
     * Serialize json to Devices array
     * @param json_devices
     * @return List of devices
     */
    public ArrayList<Device> serializeDevices(JSONArray json_devices) throws JSONException
    {
        ArrayList<Device> devices = new ArrayList<>();
        for(int i = 0;i<json_devices.length();i++)
        {
            JSONObject jo = json_devices.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String nick = jo.getString("nick");
            Company company = getCompany(jo.getInt("fk_Company"));
            LatLng location = parseLocation(jo.getString("location"));
            String hwid = jo.getString("HWID");
            String inventoryNumber = jo.getString("inventoryNumber");
            boolean ignoreAlarms = jo.getBoolean("ignoreAlarms");
            String comment = jo.getString("comment");
            DeviceStatus status = getDeviceStatus(jo.getInt("fk_DeviceStatus"));
            Manufacturer manufacturer = getManufacturer(jo.getInt("fk_Manufacturer"));
            ArrayList<Image> images = getImages(jo.getJSONArray("images"));
            int color = parseColor(jo.getString("color"));
            int primaryGroupId = parsePrimaryGroup(jo.getString("fk_PrimaryDeviceGroup"));// jo.getInt("fk_PrimaryDeviceGroup");
            boolean enabled = jo.getBoolean("enabled");
            String dateInstalledstr = jo.getString("dateInstalled");
            Date installationDate = null;
            if (dateInstalledstr != "null") {
                try {
                    //2021-06-05
                    installationDate = new SimpleDateFormat("YYYY-MM-DD").parse(dateInstalledstr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            int oldDevice_id = -1;
            try
            {
                oldDevice_id = jo.getInt("fk_DeviceOld");
            }
            catch (Exception e)
            {
                //NO old device
            }

            if(nick == "null")
            {
                nick = name;
            }


            ArrayList<DeviceType> types = getTypes(jo.getJSONArray("types"));
            devices.add(new Device(id, name, nick, company, location, hwid, inventoryNumber, ignoreAlarms, comment, status, manufacturer, oldDevice_id, images, types, color, primaryGroupId, enabled, installationDate));


        }
        this.devices = devices;
        return devices;
    }

    /**
     * Serialize DeviceType json to java object
     *
     * @param json_types JSONArray
     * @return
     */
    public ArrayList<DeviceType> serializeTypes(JSONArray json_types) throws JSONException
    {
        ArrayList<DeviceType> types = new ArrayList<>();
        for (int i = 0; i < json_types.length(); i++) {
            JSONObject jo = json_types.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String icon = jo.getString("icon");
            Domain domain = getDomain(jo.getInt("fk_Domain"));
            //Company company = getCompany(jo.getInt("fk_Company"));
            DeviceType deviceType = new DeviceType(id, name, icon, domain);
            types.add(deviceType);

        }
        this.types = types;
        return types;
    }

    /**
     * Serialize DeviceStatuses from json array
     * @param json_statuses
     * @return
     */
    public ArrayList<DeviceStatus> serializeStatuses(JSONArray json_statuses) throws JSONException
    {
        ArrayList<DeviceStatus> statuses = new ArrayList<>();
        for(int i =0;i<json_statuses.length();i++)
        {
            JSONObject jo = json_statuses.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String comment = jo.getString("comment");
            Company company = getCompany(jo.getInt("fk_Company"));
            statuses.add(new DeviceStatus(id,name,comment,company));

        }
        this.statuses = statuses;
        return statuses;
    }

    /**
     * Serialize json to DeviceGroup array
     * @param json_groups
     * @return
     */
    public ArrayList<DeviceGroup> serializeGroups(JSONArray json_groups) throws JSONException
    {
        ArrayList<DeviceGroup> groups = new ArrayList<>();
        for (int i = 0;i<json_groups.length();i++)
        {
            JSONObject jo = json_groups.getJSONObject(i);
            int id = jo.getInt("id");
            String name = jo.getString("name");
            String comment = jo.getString("comment");
            int color;
            try{
                String colorStr = jo.getString("color");
                color = Color.parseColor(colorStr);
            }
            catch (Exception e)
            {
                color = Color.WHITE;
            }
            Company company = getCompany(jo.getInt("fk_Company"));
            ArrayList<Device> devices = getDevices(jo.getJSONArray("fk_Devices"));
            groups.add(new DeviceGroup(id,name,comment,color, company,devices));

        }
        this.groups = groups;
        this.setPrimaryGroupsForDevices();
        return groups;
    }


    /**
     * Serialize json array to Domains instances
     * @param json_domains
     * @return
     */
    public ArrayList<Domain> serializeDomains(JSONArray json_domains) throws JSONException
    {
        ArrayList<Domain> domains = new ArrayList<>();
        for(int i = 0;i<json_domains.length();i++)
        {

            JSONObject domain = json_domains.getJSONObject(i);
            int id = domain.getInt("id");
            String name = domain.getString("name");
            int color = Color.parseColor(domain.getString("color"));
            //Company company = getCompany(domain.getInt("fk_Company"));
            domains.add(new Domain(id, name, color));


        }
        this.domains = domains;
        return domains;
    }


    public ArrayList<Contract> serializeContracts(JSONArray json_contracts) throws JSONException
    {
        ArrayList<Contract> contracts = new ArrayList<>();
        for (int i = 0;i<json_contracts.length();i++)
        {
            JSONObject contract = json_contracts.getJSONObject(i);
            int id = contract.getInt("id");
            String name = contract.getString("name");
            Company company = getCompany(contract.getInt("fk_Company"));
            ArrayList<Supplier> suppliers = getSuppliers(contract.getJSONArray("fk_Suppliers"));
            ArrayList<Device> devices = getDevices(contract.getJSONArray("fk_Devices"));
            contracts.add(new Contract(id,name,company,suppliers,devices));
        }
        this.contracts = contracts;
        return contracts;
    }

    /**
     * Serialize json array to Roles instances
     * @param json_roles
     * @return
     * @throws JSONException
     */
    public ArrayList<Role> serializeRoles(JSONArray json_roles) throws JSONException
    {
        ArrayList<Role> roles = new ArrayList<>();
        for(int i =0;i<json_roles.length();i++)
        {
            JSONObject role = json_roles.getJSONObject(i);
            int id = role.getInt("id");
            String name = role.getString("name");
            Company company = getCompany(role.getInt("fk_Company"));
            roles.add(new Role(id,name,company));

        }
        this.roles = roles;
        return roles;
    }

    /**
     * Serialize json array to Contacts instances
     * @param json_contacts
     * @return
     */
    public ArrayList<Contact> serializeContacts(JSONArray json_contacts) throws JSONException
    {
        ArrayList<Contact> contacts = new ArrayList<>();
        for(int i = 0;i<json_contacts.length();i++)
        {

            JSONObject contact = json_contacts.getJSONObject(i);
            int id = contact.getInt("id");
            String name = contact.getString("name");
            String tel = contact.getString("telephone");
            Role role = getRole(contact.getInt("fk_Role"));
            String email = contact.getString("email");
            JSONArray arr = contact.getJSONArray("fk_Companies");
            ArrayList<Company> fk_Companies = getCompanies(contact.getJSONArray("fk_Companies"));
            ArrayList<Contract> fk_Contracts = getContracts(contact.getJSONArray("fk_Contracts"));
            ArrayList<Supplier> fk_Suppliers = getSuppliers(contact.getJSONArray("fk_Suppliers"));
            ArrayList<Manufacturer> fk_Manufacturers = getManufacturers(contact.getJSONArray("fk_Manufacturers"));
            ArrayList<DeviceGroup> fk_DeviceGroups = getGroups(contact.getJSONArray("fk_DeviceGroups"));
            ArrayList<DeviceEvent> fk_DeviceEvents = getEvents(contact.getJSONArray("fk_DeviceEvents"));
            contacts.add(new Contact(id, name, role, email, tel, fk_Companies, fk_Contracts, fk_Suppliers, fk_Manufacturers, fk_DeviceGroups, fk_DeviceEvents));


        }
        this.contacts = contacts;
        return contacts;
    }

    /**
     * Converts "SRID=4326;POINT (14.44024715624998 50.07201075311131)" to LatLng
     * @param location
     * @return
     */
    protected LatLng parseLocation(String location)
    {
        location = location.substring(17, location.length() - 1);
        double lat = Double.valueOf(location.split(" ")[1]);
        double lng = Double.valueOf(location.split(" ")[0]);
        return new LatLng(lat,lng);
    }

    /**
     * Parse string to date
     * If parsing failed returns 1.1.2000
     * @param date
     * @return
     */
    private Date parseDate(String date)
    {
        try {
            return new SimpleDateFormat("yyyy-mm-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(2000,1,1);
    }


    /*****************************************PARSERS FOR FOREIGN KEYS*****************************************/
    /**
     * Returns company based on company id
     * @param id
     * @return
     */
    public Company getCompany(int id)
    {
        for(Company c : this.companies)
        {
            if(c.getId() == id) return c;
        }
        return null;
    }

    /**
     * Return role with given id
     * @param id
     * @return
     */
    public Role getRole(int id)
    {
        for(Role r : this.roles)
        {
            if(r.getId() == id) return r;
        }
        return null;
    }

    public Domain getDomain(int id)
    {
        assert this.domains != null;
        for(Domain d : this.domains)
        {
            if(d.getId()==id) return d;
        }
        return null;
    }

    /**
     * Return array of companies based on given array of ids
     * @param arr
     * @return
     */
    private ArrayList<Company> getCompanies(JSONArray arr)
    {
        ArrayList<Company> companies = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                companies.add(getCompany(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return companies;
    }

    /**
     * Return Supplier based on id
     * @param id
     * @return
     */
    public Supplier getSupplier(int id)
    {
        for (Supplier s : this.suppliers)
        {
            if(s.getId() == id) return s;
        }
        return null;
    }

    /**
     * Return image with given id
     * @param id
     * @return
     */
    public Image getImage(int id)
    {
        for(Image i : this.images)
        {
            if(i.getId() == id) return i;
        }
        return null;
    }

    /**
     * Return array of Images based on given array of ids
     * @param arr
     * @return
     */
    private ArrayList<Image> getImages(JSONArray arr)
    {
        ArrayList<Image> images = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                images.add(getImage(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return images;
    }


    /**
     * Return array of suppliers based on given array of ids
     * @param arr
     * @return
     */
    private ArrayList<Supplier> getSuppliers(JSONArray arr)
    {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                suppliers.add(getSupplier(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return suppliers;
    }

    /**
     * Return device with given id
     * @param id
     * @return
     */
    public Device getDevice(int id)
    {
        if(this.devices != null) {
            for (Device d : this.devices) {
                if (d.getId() == id) return d;
            }
        }
        return null;
    }


    /**
     * Returns array of devices of given ids
     * @param arr field of ids
     * @return
     */
    private ArrayList<Device> getDevices(JSONArray arr)
    {
        ArrayList<Device> devices = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                Device d = getDevice(arr.getInt(i));
                if(d!=null) devices.add(d);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return devices;
    }

    /**
     * Get Contract instance with given id
     * @param id
     * @return
     */
    public Contract getContract(int id)
    {
        for(Contract c : this.contracts)
        {
            if(c.getId()==id) return c;
        }
        return null;
    }

    public DeviceStatus getDeviceStatus(int id)
    {
        for(DeviceStatus s : this.statuses)
        {
            if(s.getId() == id) return s;
        }
        return null;
    }

    /**
     * Returns array of Contracts with given ids
     * @param arr filed of ids
     * @return
     */
    private ArrayList<Contract> getContracts(JSONArray arr)
    {
        ArrayList<Contract> a = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                a.add(getContract(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return a;
    }


    /**
     * Get manufactuer with given id
     * @param id
     * @return
     */
    public Manufacturer getManufacturer(int id)
    {
        for(Manufacturer m: this.manufacturers)
        {
            if(m.getId() == id) return m;
        }
        return null;
    }


    /**
     * Returns array of Manufacturers based on ids
     * @param arr field of ids
     * @return
     */
    private ArrayList<Manufacturer> getManufacturers(JSONArray arr)
    {
        ArrayList<Manufacturer> a = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                a.add(getManufacturer(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return a;
    }


    /**
     * Get DeviceGroup with given id
     * @param id
     * @return
     */
    public DeviceGroup getGroup(int id)
    {
        for(DeviceGroup g: this.groups)
        {
            if(g.getId() == id) return g;
        }
        return null;
    }

    /**
     * Returns array of Manufacturers based on ids
     * @param arr field of ids
     * @return
     */
    private ArrayList<DeviceGroup> getGroups(JSONArray arr)
    {
        ArrayList<DeviceGroup> a = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                a.add(getGroup(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return a;
    }

    /**
     * Return device type with given id
     * @param id
     * @return
     */
    public DeviceType getType(int id)
    {
        for(DeviceType t : this.types)
        {
            if(t.getId() == id) return t;
        }
        return null;
    }


    /**
     * Returns array of Manufacturers based on ids
     * @param arr
     * @return
     */
    private ArrayList<DeviceType> getTypes(JSONArray arr)
    {
        ArrayList<DeviceType> a = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                a.add(getType(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return a;
    }

    /**
     * Get event with given id
     * @param id
     * @return
     */
    public DeviceEvent getEvent(int id)
    {
        for(DeviceEvent e : this.events)
        {
            if(e.getId() == id) return e;
        }
        return null;
    }

    /**
     * returns array of events with given ids
     * @param arr
     * @return
     */
    private ArrayList<DeviceEvent> getEvents(JSONArray arr)
    {
        ArrayList<DeviceEvent> a = new ArrayList<>();
        for(int i = 0;i<arr.length();i++)
        {
            try {
                a.add(getEvent(arr.getInt(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return a;
    }

    /**
     * Parse color
     * @param colorStr string
     * @return parsed color or white color, when cannot parse
     */
    private int parseColor(String colorStr)
    {
        int color;
        try{
            color = Color.parseColor(colorStr);
        }
        catch (Exception e)
        {
            color = Color.WHITE;
        }
        return color;
    }

    /**
     * Set devices primary group based on its ids
     * this can be performed after loading groups
     */
    private void setPrimaryGroupsForDevices() {
        assert this.devices != null;
        assert this.groups != null;
        for(Device device : this.devices)
        {
            if(device.primaryGroupId != -1 && device.primaryGroup == null)
            {
                device.primaryGroup = getGroup(device.primaryGroupId);

            }
        }

    }

    /**
     * Parse string to Device Group id or null, if not parsed
     * @param value string value
     * @return int or null
     */
    private int parsePrimaryGroup(String value)
    {
        try
        {
            return Integer.parseInt(value);
        }
        catch (Exception e)
        {
            return -1;
        }
    }
}
