package com.giesl.centralregister.classes;

public class DeviceStatus extends RegisterModel
{

    /**
     *
     *
     "url": "http://192.168.43.3:8000/api/statuses/1/",
     "id": 1,
     "name": "active",
     "comment": "neco",
     "fk_Company": 1
     },
     *
     */

    public DeviceStatus()
    {

    }

    public DeviceStatus(int id, String name, String comment, Company company)
    {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.fk_Company = company;
    }
}
