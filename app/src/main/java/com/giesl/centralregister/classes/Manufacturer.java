package com.giesl.centralregister.classes;

import com.google.android.gms.maps.model.LatLng;

public class Manufacturer extends RegisterModel
{
    /**
     *
     *  {
     *         "url": "http://192.168.43.3:8000/api/manufacturers/1/",
     *         "id": 1,
     *         "name": "Vyrobce_1",
     *         "ICO": null,
     *         "DIC": null,
     *         "location": "SRID=4326;POINT (14.45260677539061 50.09597911219808)",
     *         "comment": "",
     *         "fk_Company": 1
     *     },
     *
     */
    private String ICO,DIC;
    private LatLng location;

    public Manufacturer()
    {

    }

    public Manufacturer(int id, String name, String ICO, String DIC, LatLng location, String comment)
    {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.ICO = ICO;
        this.DIC = DIC;
        this.location = location;

    }
}
