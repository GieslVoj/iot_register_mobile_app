package com.giesl.centralregister.classes;

import java.util.ArrayList;
import java.util.Collection;

public class RegisterModel
{
    protected int id;
    protected String name;
    protected String comment;
    protected Company fk_Company;
    protected ArrayList<Device> fk_Devices;

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getComment()
    {
        return comment;
    }

    public Company getFk_Company()
    {
        return fk_Company;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setFk_Company(Company fk_Company)
    {
        this.fk_Company = fk_Company;
    }

    public Collection<? extends Integer> getDevicesIds()
    {
        if(this.fk_Devices != null)
        {
            ArrayList<Integer> ids = new ArrayList<>();
            for(Device d : this.fk_Devices)
            {
                ids.add(d.id);
            }
            return ids;
        }
        return null;
    }
}
