package com.giesl.centralregister.classes;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

public class SensorType
{
    public int ID;
    public String name = "T";
    public String color = "cyan";
    public String domain;
    public MaterialDrawableBuilder.IconValue icon = MaterialDrawableBuilder.IconValue.NULL_ICON;


    public SensorType(int ID, String name, String color, String domain)
    {
        this.ID = ID;
        this.name = name;
        this.color = color;
        this.domain = domain;
    }

    public SensorType(int ID, String name, String color, String domain, MaterialDrawableBuilder.IconValue icon)
    {
        this.ID = ID;
        this.name = name;
        this.color = color;
        this.domain = domain;
        this.icon = icon;
    }


    public void setIcon(MaterialDrawableBuilder.IconValue icon)
    {
        this.icon = icon;
    }
}
