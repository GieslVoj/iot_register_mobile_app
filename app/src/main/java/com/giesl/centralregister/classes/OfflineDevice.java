package com.giesl.centralregister.classes;

import android.os.Environment;

import com.giesl.centralregister.adapters.OfflineFileAdapter;
import com.giesl.centralregister.constants.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class OfflineDevice extends Device
{
    public OfflineDevice(String hwid, LatLng location, String comment, Date dateInstalled)
    {
        this.HWID = hwid;
        this.location = location;

        this.name = null;
        this.ignoreAlarms = false;
        this.inventoryNumber = null;
        this.types = null;
        this.fk_DeviceStatus = null;
        this.fk_Manufacturer = null;
        this.fk_DeviceOld = null;
        this.contracts = null;
        this.groups = null;
        this.events = null;
        this.images = null;
        this.fk_Company = null;
        this.comment = comment;
        this.dateInstalled = dateInstalled;
    }

    @Override
    public int getColor()
    {
        return -1;
    }

    @Override
    public String getName()
    {
        return this.HWID;
    }


    @Override
    public String getMarkerTitleString()
    {
        return this.HWID;
    }

    @Override
    public String getSnippetString()
    {
        return this.comment;
    }

    public ArrayList<File> getOfflineImages()
    {

        return ((OfflineFileAdapter) Constants.ADAPTER).getDeviceImages(this);

    }
}
