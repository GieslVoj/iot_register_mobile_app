package com.giesl.centralregister.classes;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Company extends RegisterModel
{
    /**
     *  {
     *         "url": "http://192.168.43.3:8000/api/companies/1/",
     *         "id": 1,
     *         "name": "Společnost 1",
     *         "ICO": null,
     *         "DIC": null,
     *         "location": "SRID=4326;POINT (14.44024715624998 50.07201075311131)",
     *         "dateCreated": "2020-10-25"
     *     },
     *
     */
    private String ICO, DIC;
    private LatLng location;
    private Date dateCreated;


    public Company(int ID, String name, String ICO, String DIC, LatLng location, Date dateCreated)
    {
        this.id = ID;
        this.name = name;
        this.ICO = ICO;
        this.DIC = DIC;
        this.location = location;
        this.dateCreated = dateCreated;

    }

}
