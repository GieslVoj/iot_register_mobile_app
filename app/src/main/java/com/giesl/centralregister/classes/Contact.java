package com.giesl.centralregister.classes;

import java.util.ArrayList;

public class Contact extends RegisterModel
{
    /**
     *
     *{
     *         "url": "http://192.168.43.3:8000/api/contacts/1/",
     *         "id": 1,
     *         "name": "Name Surname",
     *         "fk_Role": 1,
     *         "email": "example@email.com",
     *         "telephone": "+420123456789",
     *         "fk_Companies": [
     *             1
     *         ],
     *         "fk_Contracts": [
     *             1
     *         ],
     *         "fk_Suppliers": [],
     *         "fk_Manufacturers": [
     *             1,
     *             2
     *         ],
     *         "fk_DeviceGroups": [],
     *         "fk_DeviceEvents": []
     *     }
     *
     */
    private String email, telephone;
    private Role fk_Role;
    private ArrayList<Company> fk_Companies = new ArrayList<>();
    private ArrayList<Contract> fk_Contracts = new ArrayList<>();
    private ArrayList<Supplier> fk_Suppliers = new ArrayList<>();
    private ArrayList<Manufacturer> fk_Manufacturers = new ArrayList<>();
    private ArrayList<DeviceGroup> fk_DeviceGroups = new ArrayList<>();
    private ArrayList<DeviceEvent> fk_DeviceEvents = new ArrayList<>();

    public Contact()
    {

    }

    public Contact(int id, String name, Role role, String email, String tel, ArrayList<Company> fk_companies, ArrayList<Contract> fk_contracts, ArrayList<Supplier> fk_suppliers, ArrayList<Manufacturer> fk_manufacturers, ArrayList<DeviceGroup> fk_deviceGroups, ArrayList<DeviceEvent> fk_deviceEvents)
    {
        this.id = id;
        this.name = name;
        this.fk_Role = role;
        this.email = email;
        this.telephone = tel;
        this.fk_Companies = fk_companies;
        this.fk_Contracts = fk_contracts;
        this.fk_Suppliers = fk_suppliers;
        this.fk_DeviceGroups = fk_deviceGroups;
        this.fk_DeviceEvents = fk_deviceEvents;
        this.fk_Manufacturers = fk_manufacturers;
    }
}
