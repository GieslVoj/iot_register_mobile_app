package com.giesl.centralregister.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.giesl.centralregister.R;
import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.adapters.EndpointAdapter;
import com.giesl.centralregister.classes.Company;
import com.giesl.centralregister.classes.DeviceType;
import com.giesl.centralregister.classes.Manufacturer;
import com.giesl.centralregister.constants.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CustomDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity activity;
    private TextView text_groups, text_types, text_events, text_manufacturers;
    private Button btn_choose_groups;
    private Button btn_choose_types;
    private Button btn_choose_events, btn_choose_manufacturers, btn_choose_companies;
    private TextView text_companies;
    private AlertDialog dialogManufacturers, dialogCompanies, dialogTypes, dialogEvents, dialogGroups;
    private EndpointAdapter adapter;
    private boolean[] checkedManufacturers;
    private ArrayList<Integer> checkedManufacturersIds;
    private boolean[] checkedCompanies;
    private ArrayList<Integer> checkedCompaniesIds;
    private boolean[] checkedTypes;
    private ArrayList<Integer> checkedTypesIds;
    private ArrayList<Integer> checkedEventsIds;
    private boolean[] checkedEvents;
    private boolean[] checkedGroups;
    private ArrayList<Integer> checkedGroupsIds;
    private Button apply;
    private RadioButton radio_all, radio_only_replaced, radio_actual_state;


    public CustomDialog(MapActivity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_dialog);
        apply = (Button) findViewById(R.id.button_apply);
        apply.setOnClickListener(this);
        this.radio_all = findViewById(R.id.radio_state_all);
        this.radio_only_replaced = findViewById(R.id.radio_state_replaced);
        this.radio_actual_state = findViewById(R.id.radio_state_actual);

        this.adapter = Constants.ADAPTER;
        initGUI();
        initDialogs();
        setListeners();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_apply:
                this.applyFilters();
                break;
            default:
                break;
        }
        dismiss();
    }

    /**
     * Send filters to Map Activity
     */
    private void applyFilters()
    {
        Constants.SHOW_MODE mode = getShowMode();
        boolean newRequest = false;
        if(Constants.FILTER_MODE != mode)
        {
            newRequest = true;
            Constants.FILTER_MODE = mode;
        }
        ((MapActivity)this.activity).applyFilters(getDevicesIds(),getCompanies(), getManufacturers(), getTypes(), newRequest);
    }

    /**
     * get show state from radio buttons
     * @return
     */
    private Constants.SHOW_MODE getShowMode()
    {
        if(this.radio_actual_state.isChecked())
        {
            return Constants.SHOW_MODE.ACTUAL;
        }
        else if (this.radio_only_replaced.isChecked())
        {
            return Constants.SHOW_MODE.ONLY_REPLACED;
        }
        else
        {
            return Constants.SHOW_MODE.ALL;
        }

    }

    /**
     * Get list of companies based on selected items
     * @return ArrayList
     */
    private ArrayList<Company> getCompanies()
    {
        ArrayList<Company> companies = new ArrayList<>();
        for(int i : this.checkedCompaniesIds)
        {
            companies.add(this.adapter.getCompany(i));
        }
        return companies;
    }

    /**
     * Get list of types based on selected items
     * @return ArrayList
     */
    private ArrayList<DeviceType> getTypes()
    {
        ArrayList<DeviceType> types = new ArrayList<>();
        for(String line : getLines(text_types))
        {
            types.add(this.adapter.getType(line));
        }
        return types;
    }

    /**
     * Get list of manufacturers based on selected items
     * @return ArrayList
     */
    private ArrayList<Manufacturer> getManufacturers()
    {
        ArrayList<Manufacturer> manufacturers = new ArrayList<>();
        for(String line : getLines(text_companies))
        {
            manufacturers.add(this.adapter.getManufacturer(line));
        }
        return manufacturers;
    }

    /**
     * Initialize all views
     */
    private void initGUI()
    {
        this.text_groups = findViewById(R.id.text_groups);
        this.text_types = findViewById(R.id.text_types);
        this.text_events = findViewById(R.id.text_events);
        this.text_manufacturers = findViewById(R.id.text_manufacturers);
        this.text_companies = findViewById(R.id.text_companies);

        this.btn_choose_groups = findViewById(R.id.button_choose_groups);
        this.btn_choose_types = findViewById(R.id.button_choose_types);
        this.btn_choose_events = findViewById(R.id.button_events);
        this.btn_choose_manufacturers = findViewById(R.id.button_choose_manufacturers);
        this.btn_choose_companies = findViewById(R.id.button_choose_companies);
    }

    /**
     * Set listeners for choosers buttons
     */
    private void setListeners()
    {
        this.btn_choose_groups.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogGroups.show();
            }
        });
        this.btn_choose_companies.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogCompanies.show();
            }
        });
        this.btn_choose_manufacturers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                dialogManufacturers.show();
            }
        });
        this.btn_choose_events.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogEvents.show();
            }
        });
        this.btn_choose_types.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogTypes.show();
            }
        });
    }

    /**
     *
     * @param options string list of names
     * @param checkedList boolean list of checked items
     * @param selectedIds Integer list of selected ids
     * @param textView TextView to render selected items
     * @return AlertDialog
     */
    private AlertDialog buildChooserDialog(@NotNull CharSequence[] options,@NotNull boolean[] checkedList, @NotNull ArrayList<Integer> selectedIds, TextView textView)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        builder.setTitle(R.string.choose)
                .setMultiChoiceItems(options,checkedList,
                        new DialogInterface.OnMultiChoiceClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked)
                            {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    if(!selectedIds.contains(which))
                                    {
                                        selectedIds.add(which);
                                    }
                                } else if (selectedIds.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    selectedIds.remove(Integer.valueOf(which));
                                }
                            }
                        }
                        )
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User clicked OK, so save the selectedItems results somewhere
                        // or return them to the component that opened the dialog
                        String names = "";
                        for(int i : selectedIds)
                        {
                            names += options[i].toString() + "\n";
                        }
                        textView.setText("");
                        textView.setText(names);
                    }
                });

        return builder.create();
    }


    /**
     * Initialize all choosers dialogs for all buttons
     */
    private void initDialogs()
    {
        //MANUFACTURERS
        CharSequence[] manufacturersNames = this.adapter.getManufacturersNames();
        this.checkedManufacturers = new boolean[manufacturersNames.length];
        this.checkedManufacturersIds = new ArrayList<Integer>();
        this.dialogManufacturers = buildChooserDialog(manufacturersNames,this.checkedManufacturers, this.checkedManufacturersIds, this.text_manufacturers);

        //COMPANIES
        CharSequence[] companiesNames = this.adapter.getCompaniesNames();
        this.checkedCompanies = new boolean[companiesNames.length];
        this.checkedCompaniesIds = new ArrayList<Integer>();
        this.dialogCompanies = buildChooserDialog(companiesNames, this.checkedCompanies, this.checkedManufacturersIds, this.text_companies);

        //TYPES
        CharSequence[] typesNames = this.adapter.getTypesNames();
        this.checkedTypes = new boolean[typesNames.length];
        this.checkedTypesIds = new ArrayList<Integer>();
        this.dialogTypes = buildChooserDialog(typesNames, this.checkedTypes, this.checkedTypesIds, this.text_types);

        //EVENTS
        CharSequence[] eventsNames = this.adapter.getEventsNames();
        this.checkedEvents = new boolean[eventsNames.length];
        this.checkedEventsIds = new ArrayList<Integer>();
        this.dialogEvents = buildChooserDialog(eventsNames, this.checkedEvents, this.checkedEventsIds, this.text_events);

        //Groups
        CharSequence[] groupsNames = this.adapter.getGroupsNames();
        this.checkedGroups = new boolean[groupsNames.length];
        this.checkedGroupsIds = new ArrayList<Integer>();
        this.dialogGroups = buildChooserDialog(groupsNames, this.checkedGroups, this.checkedGroupsIds, this.text_groups);
    }



    private ArrayList<Integer> getDevicesIds()
    {

        ArrayList<Integer> ids = new ArrayList<>();

        //GROUPS
        TextView textView = this.text_groups;
        for(String line : getLines(textView))
        {
            ids.addAll(this.adapter.getGroup(line).getDevicesIds());
        }
        //EVENTS
        textView = this.text_events;
        for(String line: getLines(textView))
        {
            ids.addAll(this.adapter.getEvent(line).getDevicesIds());
        }
        return ids;

    }

    private String[] getLines(TextView textView)
    {
        if(!this.isTextFieldEmpty(textView))
        {
            String[] lines = textView.getText().toString().split("\n");
            return lines;
        }
        return new String[]{};
    }

    /**
     * @param field TextView field
     * @return is empty
     */
    private boolean isTextFieldEmpty(@NotNull TextView field)
    {
        return field.getText().toString().equals("") || field.getText().toString().equals(" ");
    }
}