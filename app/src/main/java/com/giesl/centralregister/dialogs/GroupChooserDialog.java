package com.giesl.centralregister.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.giesl.centralregister.R;
import com.giesl.centralregister.activities.MainActivity;
import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.activities.MyCameraActivity;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.DeviceGroup;
import com.giesl.centralregister.constants.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class GroupChooserDialog extends Dialog {
    private final Context mContext;
    private Device device;
    private Button btnBack, btnSave, btnPhoto;
    private LinearLayout scrollLayout;
    private DeviceGroup primaryGroup;
    private ArrayList<DeviceGroup> newGroups = new ArrayList<>();

    /*
        Lists contains all views from single_group_layout
     */
    private ArrayList<TextView> names = new ArrayList<>();
    private ArrayList<CheckBox> checkBoxes = new ArrayList<>();
    private ArrayList<View> underlines = new ArrayList<>();
    private ArrayList<DeviceGroup> groups;


    public GroupChooserDialog(@NonNull MapActivity context) {
        super(context);
        this.mContext = context;
    }


    public GroupChooserDialog(@NotNull MapActivity mContext, Device device)
    {
        super(mContext);
        this.mContext = mContext;
        this.device = device;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.initGUI();

    }



    /**
     * Initialize GUI elements of dialog
     */
    private void initGUI()
    {
        setContentView(R.layout.group_chooser_dialog);
        this.btnBack = findViewById(R.id.button_back);
        this.btnSave = findViewById(R.id.button_save);
        this.btnPhoto = findViewById(R.id.button_photo);
        this.scrollLayout = findViewById(R.id.scrollLayout);
        this.showGroups();
        if(this.device != null)
        {
            this.setActiveGroups(this.device);
            this.setPrimaryGroup(this.device);
        }

        setListeners();
    }

    /**
     * Set underline for primary group
     * @param device
     */
    private void setPrimaryGroup(Device device)
    {
        DeviceGroup group = device.getPrimaryGroup();
        if(group != null)
        {
            int idx = this.groups.indexOf(group);
            if(idx != -1)
            {
                this.primaryGroup = group;
                this.underlines.get(idx).setBackgroundColor(group.getColor());
            }
        }
    }

    /**
     * Set checkboxes to selected groups based on device
     */
    private void setActiveGroups(@NotNull Device device)
    {
        assert this.groups != null;
        ArrayList<DeviceGroup> deviceGroups = device.getGroups();
        this.newGroups = deviceGroups;
        for(DeviceGroup group : deviceGroups)
        {

            int idx = this.groups.indexOf(group);
            if(idx != -1)
            {
                this.checkBoxes.get(idx).setChecked(true);
            }
        }
    }

    /**
     * Init Device Groups to scrollLayout and init all lists
     */
    private void showGroups()
    {
        this.groups = Constants.ADAPTER.getGroups();
        for(DeviceGroup group : this.groups)
        {
            View view = View.inflate(this.mContext, R.layout.single_group_layout,null);
            TextView tv = view.findViewById(R.id.textViewGroupName);
            CheckBox ch = view.findViewById(R.id.groupCheckbox);
            View v = view.findViewById(R.id.textUnderline);
            tv.setText(group.getName());
            this.names.add(tv);
            this.checkBoxes.add(ch);
            this.underlines.add(v);

            // SET Listeners for views
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int idx = names.indexOf(v);
                    if(idx != -1)
                    {
                        DeviceGroup selectedGroup = groups.get(idx);
                        View underline = underlines.get(idx);
                        CheckBox checkBox = checkBoxes.get(idx);
                        if(selectedGroup.equals(primaryGroup))
                        {
                            primaryGroup = null;
                            underline.setBackgroundColor(Color.TRANSPARENT);
                        }
                        else if(primaryGroup == null)
                        {
                            underline.setBackgroundColor(selectedGroup.getColor());
                            checkBox.setChecked(true);
                            primaryGroup = selectedGroup;

                        }
                        else
                        {
                            int idx_primary = groups.indexOf(primaryGroup);
                            underlines.get(idx_primary).setBackgroundColor(Color.TRANSPARENT);
                            primaryGroup = selectedGroup;
                            underline.setBackgroundColor(selectedGroup.getColor());
                            checkBox.setChecked(true);

                        }
                    }
                }
            });

            ch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int idx = checkBoxes.indexOf(view);
                    DeviceGroup selectedGroup = groups.get(idx);
                    //when unchecked primary group -> remove primary group and uncolored underline
                    if (!((CompoundButton) view).isChecked()) {
                        if (selectedGroup != null) {
                            newGroups.remove(group);
                            if (selectedGroup.equals(primaryGroup)) {
                                primaryGroup = null;
                                underlines.get(idx).setBackgroundColor(Color.TRANSPARENT);
                            }
                        }
                    }
                    // checked == true
                    else
                    {
                        newGroups.add(selectedGroup);
                    }
                }
            });



            this.scrollLayout.addView(view);
        }

    }

    /**
     * Set listeners for GUI elements
     */
    private void setListeners()
    {
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        this.btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mContext, MyCameraActivity.class);
                myIntent.putExtra("deviceID", device.getId());
                Constants.mContext = null;
                mContext.startActivity(myIntent);
            }
        });
    }

    private void back() {
        this.dismiss();
        super.onBackPressed();
    }
    
    private void save()
    {
        device.setGroups(newGroups);
        if(primaryGroup != null)
        {
            device.setPrimaryDeviceGroup(primaryGroup);
        }
        ((MapActivity) this.mContext).saveDevice(device);
        this.dismiss();
    }




}
