package com.giesl.centralregister.interfaces;

import android.view.KeyEvent;

public interface IActivity
{

    void initGUI();
    void setListeners();
    void onBackPressed();
    boolean onKeyDown(int keyCode, KeyEvent event);


    public void asyncCallback();

}
