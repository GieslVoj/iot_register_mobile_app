package com.giesl.centralregister.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.classes.Company;
import com.giesl.centralregister.classes.Contract;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.DeviceEvent;
import com.giesl.centralregister.classes.DeviceGroup;
import com.giesl.centralregister.classes.DeviceStatus;
import com.giesl.centralregister.classes.DeviceType;
import com.giesl.centralregister.classes.Image;
import com.giesl.centralregister.classes.Manufacturer;
import com.giesl.centralregister.classes.User;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public abstract class EndpointAdapter
{
    private static final String TAG = "ENDPOINT_ADAPTER";
    public User user;

    public abstract boolean authenticate(String toString, String toString1);

    public abstract String[] getGroupsNames();

    public abstract String[] getManufacturersNames();

    public abstract String[] getStatusesNames();

    public abstract String[] getCompaniesNames();

    public abstract String[] getDevicesNames();

    public abstract DeviceGroup getGroup(String groupName);

    public abstract Device getDevice(String name);

    public abstract Device getDevice(int id);

    public abstract Manufacturer getManufacturer(String name);

    public abstract DeviceStatus getStatus(String name);

    public abstract DeviceType getType(String name);

    public abstract Contract getContract(String name);

    public abstract DeviceEvent getEvent(String name);

    public abstract Company getCompany(String name);

    public abstract CharSequence[] getContractsNames();

    public abstract CharSequence[] getEventsNames();

    public abstract CharSequence[] getTypesNames();

    public abstract void putDevice(Device device, Context context);

    public abstract Device getDeviceByHWID(String deviceHWID);

    public abstract boolean isDataLoaded();

    public abstract void loadAsync();

    public abstract void loadAsync(LatLng lastLocation, float radius);

    public abstract ArrayList<? extends Device> getDevices();

    public abstract ArrayList<DeviceGroup> getGroups();

    public abstract void postDevice(Device device, MapActivity activity);

    public abstract Company getCompany(int id);

    public abstract Device getReplacementForDevice(Device selectedDevice);

    public abstract boolean getUserInfo();

    public abstract boolean isUserStaff();

    public abstract int saveImage(File image, Device device) throws ExecutionException, InterruptedException;

    public abstract Image getImage(int id);

    public abstract boolean updateObject(String imageUrl, JSONObject json);

    public abstract boolean setUserSelectedCompany(int companyId);


    protected class Query
    {
        public String url;
        public String method = HttpMethod.GET;
        public String postData = "";

        public Query()
        {

        }

        public Query(String query, String method)
        {
            this.url = query;
            this.method = method;
        }

        public Query(String query, String method, String postData)
        {
            this.url = query;
            this.method = method;
            this.postData = postData;
        }
    }

    /**
     * Response class
     */
    protected static class Response
    {

        public String response;
        public int returnCode = -1;

        public Response()
        {
        }
    }

    /**
     * Checks if response code is 2xx
     * @param code
     * @return is 2xx response code
     */
    protected static boolean isReturnCodeOK(int code)
    {
        return (code >= 200 && code <= 300);
    }

    /** Method for check connection to BASE_URL server
     *
     * @return true if network is available
     */
    public static boolean isNetworkAvailable()
    {
        try {
            // wait for async task
            boolean value = new CheckConnection().execute().get();
            return value;
        } catch (ExecutionException | InterruptedException e) {
            return false;
        }
    }

    /**
     * Async class for checking connection
     */
    protected static class CheckConnection extends AsyncTask<Void,Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... voids)
        {
            try {
                URL url = new URL(Constants.BASE_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                urlConnection.disconnect();
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Exception", e);
            }
            return false;
        }
    }

    /**
     * Class for send data to BASE_URL and gets response code
     */
    private static class SendData extends AsyncTask<Query, Void, Response>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        /**
         * @param Query
         */
        protected Response doInBackground(Query... queries) {

            HttpURLConnection httpURLConnection = null;
            BufferedReader reader = null;
            Response response = new Response();
            try {

                Query query = (Query) queries[0];
                URL url = new URL(Constants.BASE_URL + query.url);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod(query.method);
                httpURLConnection.setDoOutput(true);


                // DETERMINE IF JWT IS SET
                if (Constants.JWT != null)
                {
                    String token = Constants.JWT;
                    addAuthenticationHeader(httpURLConnection, token);
                }

                if(query.postData != "") {
                    httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                    DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                    wr.writeBytes(query.postData);
                    wr.flush();
                    wr.close();
                }
                httpURLConnection.connect();
                response.returnCode = httpURLConnection.getResponseCode();

                //FIXME
                if(isReturnCodeOK(response.returnCode)) {
                    InputStream stream = httpURLConnection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                        Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                    }
                    response.response = buffer.toString();
                    return response;
                }
                else {
                    Log.e(TAG, "Bad return code: " + httpURLConnection.getResponseCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return response;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(new Response());
            Log.e("Receive data", result); // this is expecting a response code to be sent from your server upon receiving the POST data

        }
    }

    /**
     * Send query
     * If working mode is not ONLINE returns null
     * @param query query
     * @return response
     */
    protected Response sendQuery(Query query)
    {
        if(!(Constants.WORKING_MODE == WORKING_MODES.ONLINE))
        {
            return null;
        }
        try {
            String normalizedQuery = Normalizer.normalize(query.url, Normalizer.Form.NFD);
            normalizedQuery = normalizedQuery.replaceAll("[^\\p{ASCII}]", "");
            if(!normalizedQuery.endsWith("/"))
            {
                normalizedQuery += "/";
            }
            query.url = normalizedQuery;
            Response response = new SendData().execute(query).get();
            return response;
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG,e.getMessage());
            return null;
        }
    }

    /**
     * Add the HTTP Bearer authentication header that must be present on every API call.
     *
     * @param restApiURLConnection The open connection to the REST API.
     */
    private static void addAuthenticationHeader(HttpURLConnection restApiURLConnection, String accessToken) {
        //accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjA0NTk1MjQ4LCJlbWFpbCI6IiJ9.Kd8CSVg8jII4Oohdps898-sjmz7GxfxJzQbrq3UZvk8";
        restApiURLConnection.setRequestProperty("Authorization", "JWT " + accessToken);
    }

    //WRITING TO SD CARD

    protected void checkExternalMedia(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            Toast.makeText(Constants.mContext,"External Media: readable="
                    + mExternalStorageAvailable +" writable="+ mExternalStorageWriteable,Toast.LENGTH_SHORT).show();
        }

    }


    protected void writeToSDFile(String action, String msg){

        // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

        File root = android.os.Environment.getExternalStorageDirectory();

        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

        File dir = new File (root.getAbsolutePath() + "/iotRegister");
        dir.mkdirs();
        File file = new File(dir, "logs.txt");

        try {
            String date = new SimpleDateFormat("dd-M-yyyy hh:mm:ss z").format(new Date());
            String data = String.format("%s;%s;%s",date,action,msg);

            FileOutputStream f = new FileOutputStream(file,true);
            PrintWriter pw = new PrintWriter(f);
            pw.println(data);
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("WRITE", "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(Constants.mContext,"Device log into file: "+file,Toast.LENGTH_SHORT).show();
    }

    protected String[] readLinesFromFile(String filename)
    {
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File (root.getAbsolutePath() + "/iotRegister");
        File file = new File(dir, filename);
        return getLines(file);
    }

    private String[] getLines(File file)
    {
        if(file.exists())
        {
            //Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            }
            catch (IOException e) {
                //You'll need to add proper error handling here
            }
            return text.toString().split("\n");
        }
        else
        {
            return new String[0];
        }
    }

    protected String[] readLinesFromFile(File file)
    {
        return getLines(file);
    }

    public abstract Bitmap loadImage(String uri) throws InterruptedException, ExecutionException;
}
