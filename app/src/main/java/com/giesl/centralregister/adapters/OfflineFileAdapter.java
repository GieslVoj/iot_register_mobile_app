package com.giesl.centralregister.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.classes.Company;
import com.giesl.centralregister.classes.Contract;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.DeviceEvent;
import com.giesl.centralregister.classes.DeviceGroup;
import com.giesl.centralregister.classes.DeviceStatus;
import com.giesl.centralregister.classes.DeviceType;
import com.giesl.centralregister.classes.Image;
import com.giesl.centralregister.classes.Manufacturer;
import com.giesl.centralregister.classes.OfflineDevice;
import com.giesl.centralregister.constants.Constants;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;


public class OfflineFileAdapter extends EndpointAdapter
{
    ArrayList<OfflineDevice> devices;
    private String filename = Constants.LOG_FILE_OFFLINE;
    protected String DATE_FORMAT = "dd-M-yyyy hh:mm:ss z";

    @Override
    public boolean authenticate(String toString, String toString1)
    {
        return true;
    }

    @Override
    public String[] getGroupsNames()
    {
        return null;
    }

    @Override
    public String[] getManufacturersNames()
    {
        return null;
    }

    @Override
    public String[] getStatusesNames()
    {
        return null;
    }

    @Override
    public String[] getCompaniesNames()
    {
        return null;
    }

    @Override
    public String[] getDevicesNames()
    {
        return null;
    }

    @Override
    public DeviceGroup getGroup(String groupName)
    {
        return null;
    }

    @Override
    public Device getDevice(String name)
    {
        for(Device d: this.devices)
        {
            if(d.getName().equals(name)) return d;
        }
        return null;
    }

    @Override
    public Device getDevice(int id) {

        return null;
    }

    @Override
    public Manufacturer getManufacturer(String name)
    {
        return null;
    }

    @Override
    public DeviceStatus getStatus(String name)
    {
        return null;
    }

    @Override
    public DeviceType getType(String name)
    {
        return null;
    }

    @Override
    public Contract getContract(String name)
    {
        return null;
    }

    @Override
    public DeviceEvent getEvent(String name)
    {
        return null;
    }

    @Override
    public Company getCompany(String name)
    {
        return null;
    }

    @Override
    public CharSequence[] getContractsNames()
    {
        return null;
    }

    @Override
    public CharSequence[] getEventsNames()
    {
        return null;
    }

    @Override
    public CharSequence[] getTypesNames()
    {
        return null;
    }

    @Override
    public void putDevice(Device device, Context context)
    {
        Constants.mContext = context;
        writeToSDFile(device, this.filename);
        Constants.IS_ADDING_DEVICE = true;
        Constants.RESPONSE_CODE = 200;
        Constants.ACTIVITY.asyncCallback();

    }

    @Override
    public Device getDeviceByHWID(String deviceHWID)
    {
        if(this.devices != null)
        {
            for(Device d : this.devices)
            {
                if(d.getHWID().equals(deviceHWID)) return d;
            }
        }
        String date = new SimpleDateFormat("dd-M-yyyy hh:mm:ss z").format(new Date());
        return new OfflineDevice(deviceHWID,Constants.DEFAULT_LOCATION, date, null);
    }

    @Override
    public boolean isDataLoaded()
    {
        return true;
    }

    @Override
    public void loadAsync()
    {
        this.devices = openFile(getOfflineFile());
        Constants.ACTIVITY.asyncCallback();
    }

    public ArrayList<OfflineDevice> openFile(File file)
    {
        ArrayList<OfflineDevice> devices = new ArrayList<>();
        if(!file.canRead()) return devices;

        String[] lines = readLinesFromFile(file);
        return serializeLines(lines);
    }

    /**
     * Make list of offline devices from string[] lines
     * @param lines
     * @return
     */
    private ArrayList<OfflineDevice> serializeLines(String[] lines)
    {
        ArrayList<OfflineDevice> devices = new ArrayList<>();
        for(String line : lines)
        {
            try
            {
                OfflineDevice device = serializeOfflineDevice(line);
                if(devices.contains(device))
                {
                    //multiple records in log(repositioning etc.)
                    devices.get(this.devices.indexOf(device)).setLocation(device.getLocation());
                }
                else {
                    devices.add(device);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return devices;
    }

    private OfflineDevice serializeOfflineDevice(String line)
    {
        String[] parts = line.split(";");
        Date installedDate = null;
        try {


            installedDate = new SimpleDateFormat(DATE_FORMAT).parse(parts[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(parts.length >= 2) {
            LatLng location = parseLocation(parts[2]);
            String comment = parts[0];
            return new OfflineDevice(parts[1], location, comment, installedDate);
        }
        else{
            return null;
        }
    }

    private LatLng parseLocation(String string)
    {
        double lat = Double.valueOf(string.split(",")[0]);
        double lng = Double.valueOf(string.split(",")[1]);
        return new LatLng(lat,lng);
    }

    public void importFile(File file)
    {
        file.getParent();

        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File (root.getAbsolutePath() + "/" + Constants.LOG_FILE_DIR);
        File file2 = new File(dir, "logs_offline_import.txt");



        if (file2 != null)
        {
            ArrayList<OfflineDevice> imported = openFile(file2);
            this.devices.addAll(imported);
            Constants.ACTIVITY.asyncCallback();
        }
    }

    public void importDataFromUri(Uri uri)
    {
        BufferedReader br;
        ArrayList<String> lines = new ArrayList<>();
        try {

            br = new BufferedReader(new InputStreamReader(Constants.mContext.getContentResolver().openInputStream(uri)));
            String line = null;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] linesarr = lines.toArray(new String[lines.size()]);
        ArrayList<OfflineDevice> devices = serializeLines(linesarr);
        this.devices.addAll(devices);
        Constants.ACTIVITY.asyncCallback();
    }

    @Override
    public void loadAsync(LatLng lastLocation, float v)
    {
        loadAsync();
    }

    @Override
    public ArrayList<? extends Device> getDevices()
    {
        return this.devices;
    }

    @Override
    public ArrayList<DeviceGroup> getGroups()
    {
        return null;
    }

    @Override
    public void postDevice(Device device, MapActivity activity)
    {
        this.putDevice(device, activity);
    }

    @Override
    public Company getCompany(int id)
    {
        return null;
    }

    @Override
    public Device getReplacementForDevice(Device selectedDevice)
    {
        return null;
    }

    @Override
    public boolean getUserInfo() {
        return false;
    }

    @Override
    public boolean isUserStaff() {
        return false;
    }

    protected File getOfflineFile()
    {
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File (root.getAbsolutePath() + "/" + Constants.LOG_FILE_DIR);
        File file = new File(dir, Constants.LOG_FILE_OFFLINE);
        return file;
    }

    protected void writeToSDFile(Device device, String filename)
    {

        // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

        File root = android.os.Environment.getExternalStorageDirectory();

        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

        File dir = new File(root.getAbsolutePath() + "/" + Constants.LOG_FILE_DIR);
        dir.mkdirs();
        File file = new File(dir, filename);

        try {
            String date = new SimpleDateFormat(DATE_FORMAT).format(new Date());
            String location = String.valueOf(device.getLocation().latitude) + "," + String.valueOf(device.getLocation().longitude);
            String data = String.format("%s;%s;%s", date, device.getHWID(), location);

            FileOutputStream f = new FileOutputStream(file, true);
            PrintWriter pw = new PrintWriter(f);
            pw.println(data);
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("WRITE", "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(Constants.mContext, "Device log into file: " + file, Toast.LENGTH_SHORT).show();
    }


    /**
     * Loads devices from log_offline file
     * @return arrayList of devices
     */
    public ArrayList<OfflineDevice> getDevicesFromLogFile()
    {
        String[] lines = readLinesFromFile(Constants.LOG_FILE_OFFLINE);
        ArrayList<OfflineDevice> devices = new ArrayList<>();
        for(String line : lines)
        {
            OfflineDevice device = serializeOfflineDevice(line);
            if(!devices.contains(device))
            {
                devices.add(serializeOfflineDevice(line));
            }
            else
            {
                //Device equals only comparing hwid
                devices.remove(device);
                devices.add(device);

            }

        }
        return devices;
    }

    @Override
    public Bitmap loadImage(String uri) throws InterruptedException, ExecutionException {
        // TODO
        return null;
    }

    @Override
    public int saveImage(File image, Device device) {
        // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

        File root = android.os.Environment.getExternalStorageDirectory();

        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

        File dir = new File(root.getAbsolutePath() + "/" + Constants.LOG_FILE_DIR + "/" + Constants.OFFLINE_IMAGES_DIR);
        dir.mkdirs();

        String filename = device.getName() + "-" + image.getName();
        File file = new File(dir, filename);

        try {
            copyFile(image, file);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }

        return 1;
    }

    public ArrayList<File> getDeviceImages(Device device)
    {
        ArrayList<File> files = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().toString()+ "/" + Constants.LOG_FILE_DIR + "/" + Constants.OFFLINE_IMAGES_DIR;
        File directory = new File(path);
        File[] images = directory.listFiles();
        if(images != null) {
            for (int i = 0; i < images.length; i++) {
                String name = images[i].getName();
                if (name.startsWith(device.getName())) files.add(images[i]);
            }
        }

        return files;
    }

    @Override
    public Image getImage(int id) {
        return null;
    }

    @Override
    public boolean updateObject(String imageUrl, JSONObject json) {
        return false;
    }

    @Override
    public boolean setUserSelectedCompany(int companyId) {
        return false;
    }


    /**
     * Copy file
     * @param src File
     * @param dst File
     * @throws IOException
     */
    public static void copyFile(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }


}
