package com.giesl.centralregister.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.giesl.centralregister.R;
import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.classes.Company;
import com.giesl.centralregister.classes.Contact;
import com.giesl.centralregister.classes.Contract;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.DeviceEvent;
import com.giesl.centralregister.classes.DeviceGroup;
import com.giesl.centralregister.classes.DeviceStatus;
import com.giesl.centralregister.classes.DeviceType;
import com.giesl.centralregister.classes.Domain;
import com.giesl.centralregister.classes.Image;
import com.giesl.centralregister.classes.Manufacturer;
import com.giesl.centralregister.classes.RegisterModel;
import com.giesl.centralregister.classes.Role;
import com.giesl.centralregister.classes.Serializer;
import com.giesl.centralregister.classes.Supplier;
import com.giesl.centralregister.classes.User;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.Tags;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.giesl.centralregister.scanner.FullScannerActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.sleep;

public class ApiAdapter extends EndpointAdapter
{
    private static final String TAG = "API_ADAPTER";
    private ArrayList<Device> devices;
    private ArrayList<DeviceType> types;
    private ArrayList<Company> companies;
    private ArrayList<Domain> domains;
    private ArrayList<Contact> contacts;
    private ArrayList<Role> roles;
    private ArrayList<Contract> contracts;
    private ArrayList<Supplier> suppliers;
    private ArrayList<Manufacturer> manufacturers;
    private ArrayList<DeviceGroup> groups;
    private ArrayList<DeviceStatus> statuses;
    private ArrayList<DeviceEvent> events;
    private ArrayList<Image> images;
    private JSONArray resultResponse;
    private ArrayList<JSONResponse> arr;
    private Serializer serializer;



    public ApiAdapter()
    {

    }


    /**
     * If Device is replacement for another device, set fk_DeviceOld to device
     */
    private void parseOldDevices()
    {
        //Device old
        for (Device d : this.devices) {
            if (d.getFk_DeviceOld_id() != -1) {
                d.setOldDevice(getDevice(d.getFk_DeviceOld_id()));
            }
        }
    }


    /**
     * Authenticate credentials to API server and set get JWT
     *
     * @param username username
     * @param password password
     * @return boolean if success
     */
    public boolean authenticate(String username, String password)
    {
        try {
            return this.attemptLogin(username, password);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Atempt to authenticate with given username and password
     *
     * @param username username
     * @param password password
     * @return boolean if success
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private boolean attemptLogin(String username, String password) throws ExecutionException, InterruptedException, TimeoutException
    {
        final boolean[] success = {false};
        if(username == null || username.equals("") || password == null || password.equals(""))
        {
            return false;
        }
        String url = Constants.BASE_URL + Constants.API_JWT;
        Ion.getDefault(Constants.mContext).getConscryptMiddleware().enable(true);
        Ion.with(Constants.mContext)
                .load(HttpMethod.POST, url)
                .setBodyParameter("username", username)
                .setBodyParameter("password", password)
                .asString()
                .setCallback(new FutureCallback<String>()
                {
                    @Override
                    public void onCompleted(Exception e, String result)
                    {
                        if (e == null) {
                            JSONObject o = null;
                            try {
                                o = new JSONObject(result);
                            String error = o.optString("error");
                            String token = o.optString("token");
                            if (!token.equals("")) {
                                Constants.JWT = token;
                                success[0] = true;
                            } else {
                                //Show error
                                success[0] = false;
                            }
                            } catch (JSONException jsonException) {
                                jsonException.printStackTrace();
                                success[0] = false;
                            }
                        } else {

                        }


                    }
            }).get(10, TimeUnit.SECONDS);
        return success[0];
    }

    /**
     * Get adapter list of devices
     * @return List of devices
     */
    public ArrayList<Device> getDevices()
    {
        return this.devices;
    }

    /**
     * @return groups names as list
     */
    public String[] getGroupsNames()
    {
        if(this.groups == null) return new String[]{""};

        String[] arr = new String[this.groups.size()];
        int index = 0;
        for(DeviceGroup g : this.groups)
        {
            arr[index] = g.getName();
            index++;
        }

        return arr;

    }

    /**
     * Returns types names as string arr
     * @return list of names
     */
    public String[] getTypesNames()
    {
        if(this.types == null) return new String[]{""};

        String[] arr = new String[this.types.size()];
        int index = 0;
        for(DeviceType g : this.types)
        {
            arr[index] = g.getName();
            index++;
        }

        return arr;
    }

    public DeviceGroup getGroup(String name)
    {
        for(DeviceGroup g: this.groups)
        {
            if(g.getName().equals(name)) return g;
        }
        return null;
    }

    /**
     * Return statuses names as String list
     * @return list of names
     */
    public String[] getStatusesNames()
    {
        if(this.statuses == null) return new String[]{""};

        String[] arr = new String[this.statuses.size()];
        int index = 0;
        for(DeviceStatus s : this.statuses)
        {
            arr[index] = s.getName();
            index++;
        }

        return arr;
    }

    /**
     * Retruns manufacturers names as list
     * @return
     */
    public String[] getManufacturersNames()
    {
        if(this.manufacturers == null) return new String[]{""};
        String[] arr = new String[this.manufacturers.size()];
        int index = 0;
        for(RegisterModel c : this.manufacturers)
        {
            arr[index] = c.getName();
            index++;
        }

        return arr;
    }

    /**
     * Returns companies names as string list
     * @return
     */
    public String[] getCompaniesNames()
    {
        if(this.companies == null) return new String[]{""};
        String[] arr = new String[this.companies.size()];
        int index = 0;
        for(Company c : this.companies)
        {
            arr[index] = c.getName();
            index++;
        }

        return arr;
    }

    /**
     * Returns names of devices as list
     * @return list
     */
    public String[] getDevicesNames()
    {
        String[] arr = new String[this.devices.size()];
        int idx = 0;
        for(Device d : this.devices)
        {
            arr[idx] = d.getName();
            idx++;
        }
        Arrays.sort(arr);
        return arr;
    }

    /**
     * Returns names of contracts as list
     * @return
     */
    public String[] getContractsNames()
    {
        if(this.contracts == null) return new String[]{""};
        String[] arr = new String[this.contracts.size()];
        int index = 0;
        for(RegisterModel c : this.contracts)
        {
            arr[index] = c.getName();
            index++;
        }

        return arr;
    }

    /**
     *
     * @return
     */
    public String[] getEventsNames()
    {
        if(this.events == null) return new String[]{""};
        String[] arr = new String[this.events.size()];
        int index = 0;
        for(DeviceEvent e : this.events)
        {
            String name = e.getName();
            arr[index] = name;
            index++;
        }
        return arr;

    }

    public boolean isDevicesLoaded()
    {
        return this.devices!=null;
    }

    /**
     *Returns if all data is loaded
     * @return no model is null
     */
    public boolean isDataLoaded()
    {
       ArrayList<Object> models = new ArrayList<>();
       models.add(this.devices);
       models.add(this.companies);
       models.add(this.types);
       models.add(this.domains);
       models.add(this.contacts);
       models.add(this.roles);
       models.add(this.contracts);
       models.add(this.suppliers);
       models.add(this.manufacturers);
       models.add(this.groups);
       models.add(this.statuses);
       models.add(this.events);
       models.add(this.images);

       return !models.contains(null);
    }


    /******************************************************************************************/

    public void loadAsync()
    {
        new BackgroundTaskGet().execute();
    }

    public void loadAsync(LatLng position, float radius)
    {
        new BackgroundTaskGet(position, radius).execute();
    }

    /**
     *
     * @param name name of type
     * @return device type
     */
    public DeviceType getType(String name)
    {
        return (DeviceType) this.getModel(name, this.types);
    }

    /**
     *
     * @param name name of status
     * @return Device Status
     */
    public DeviceStatus getStatus(String name)
    {
        return (DeviceStatus) this.getModel(name, this.statuses);
    }

    /**
     * @param name name of manufacturer
     * @return Manufacturer or null
     */
    public Manufacturer getManufacturer(String name)
    {
        return (Manufacturer) this.getModel(name, this.manufacturers);
    }

    /**
     *
     * @param name name of device
     * @return device or null
     */
    public Device getDevice(String name)
    {
        return (Device) this.getModel(name, this.devices);
    }

    /**
     * Get device by id
     * @param id id of device
     * @return Device or null if not found
     */
    public Device getDevice(int id)
    {
        return (Device) this.getModel(id, this.devices);
    }

    /**
     * Get device by HWID
     * @param deviceHWID hwid String
     * @return Device or null
     */
    public Device getDeviceByHWID(String deviceHWID)
    {
        assert this.devices != null;
        for(Device d : this.devices)
        {
            if(d.getHWID().equals(deviceHWID)) return d;
        }
        Device device = getDeviceByHWIDFromAPI(deviceHWID);
        if(device != null)
        {
            return device;
        }
        return null;
    }

    /**
     * Get device with given hwid from API
     * @param deviceHWID hwid of device
     * @return device or null
     */
    private Device getDeviceByHWIDFromAPI(String deviceHWID)
    {
        final Device[] device = {null};
        String url = Constants.BASE_URL + Constants.API_DEVICES + "/?hwid=" + deviceHWID.replace("&","%26");
        url = url.replace(" ", "%20");
        try {
            Ion.with(Constants.mContext)
                    .load(HttpMethod.GET, url)
                    .addHeader("Authorization", "JWT " + Constants.JWT)
                    .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                    .asString(Charset.forName("UTF-8"))
                    .withResponse()
                    .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>()
                    {
                        @Override
                        public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                        {
                            try{
                                if (result == null) {
                                    if(attemptLogin(Constants.USER_USERNAME, Constants.USER_PASSWORD))
                                    {
                                        device[0] = getDeviceByHWIDFromAPI(deviceHWID);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                JSONArray arr = new JSONArray(new String(result.getResult().getBytes(), "UTF-8"));
                                assert arr != null;
                                assert serializer != null;
                                ArrayList<Device> devices = serializer.serializeDevices(arr);
                                if (devices.size() == 1) {
                                    device[0] = devices.get(0);
                                }

                            } catch (Exception exception) {
                                return;
                            }
                        }

                    }).get(5, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
            return null;
        }
        return device[0];
    }

    /**
     * Get replacemet for given device
     * @param device device, that has to be replaced
     * @return device or null, if device has no replacement
     */
    private Device getDeviceReplacementFromAPI(Device device)
    {
        String url = Constants.BASE_URL + Constants.API_DEVICES + "/?fk_device_old=" + device.getId();
        return getSingleDeviceFromAPi(url);
    }


    /**
     * Get single device from api
     * @param url url to api with optionals parameters
     * @return device or null if not found
     */
    private Device getSingleDeviceFromAPi(String url)
    {
        final Device[] device = {null};
        try {
            Ion.with(Constants.mContext)
                    .load(HttpMethod.GET, url)
                    .addHeader("Authorization", "JWT " + Constants.JWT)
                    .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                    .asString(Charset.forName("UTF-8"))
                    .withResponse()
                    .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>()
                    {
                        @Override
                        public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                        {
                            try{
                                //when session is expired reload login and call itself
                                if (result == null) {
                                    if(attemptLogin(Constants.USER_USERNAME, Constants.USER_PASSWORD))
                                    {
                                        device[0] = getSingleDeviceFromAPi(url);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                JSONArray arr = new JSONArray(new String(result.getResult().getBytes(), "UTF-8"));
                                assert arr != null;
                                assert serializer != null;
                                ArrayList<Device> devices = serializer.serializeDevices(arr);
                                if (devices.size() == 1) {
                                    device[0] = devices.get(0);
                                }

                            } catch (Exception exception) {
                                return;
                            }
                        }

                    }).get(5, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
            return null;
        }
        return device[0];
    }

    /**
     *
     * @param name name of contract
     * @return Contract or null
     */
    public Contract getContract(String name)
    {
        return (Contract) this.getModel(name, this.contracts);
    }

    /**
     *
     * @param name Company name
     * @return Company or null
     */
    public Company getCompany(String name)
    {
        return (Company) this.getModel(name,this.companies);
    }

    @Override
    public Company getCompany(int id) {
        return (Company) this.getModel(id, this.companies);
    }

    @Override
    public Image getImage(int id) {
        return (Image) getModel(id, this.images);
    }

    /**
     *
     * @param name - event name
     * @return DeviceEvent or null
     */
    public DeviceEvent getEvent(String name)
    {
        return (DeviceEvent) this.getModel(name, this.events);
    }

    /**
     *
     * @param name name of model instance
     * @param arr Array where to lookup
     * @return RegisterModel instance or null
     */
    private RegisterModel getModel(String name, ArrayList<? extends RegisterModel> arr)
    {
        if(arr == null) return null;
        for(RegisterModel model : arr)
        {
            if(model.getName().equals(name)) return model;
        }
        return null;
    }

    /**
     *
     * @param id id of model instance
     * @param arr Array to lookup
     * @return RegisterModel instance or null
     */
    private RegisterModel getModel(int id, ArrayList<? extends RegisterModel> arr)
    {
        if(arr == null) return null;
        for(RegisterModel model : arr)
        {
            if(model.getId() == id) return model;
        }
        return null;
    }

    /**
     * Send PUT request to rest api
     * @param device device
     */
    public void putDevice(@NotNull Device device, Context context)
    {
        Constants.mContext = context;
        try {
            JSONObject device_json = device.get_json();
            Query query = new Query();
            query.method = HttpMethod.PUT;
            query.url = String.format("%s/%d", Constants.API_DEVICES, device.getId());

            if(context instanceof FullScannerActivity)
            {
                new BackgroundTask(query,device_json, true).execute();
            }
            else {
                new BackgroundTask(query, device_json).execute();
            }
            writeToSDFile(Tags.tag.EDIT,device_json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Sends post request to rest api with device
     * @param device
     * @param context
     */
    public void postDevice(Device device, Context context)
    {
        Constants.mContext = context;
        try {
            JSONObject device_json = device.get_json();
            Query query = new Query();
            query.method = HttpMethod.POST;
            query.url = Constants.API_DEVICES;
            new BackgroundTask(query, device_json).execute();
            checkExternalMedia();
            writeToSDFile(Tags.tag.ADD,device_json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get device that replaces given device
     * @param tmpDevice device to check
     * @return Device or null if replacement doesn't exists
     */
    public Device getReplacementForDevice(Device tmpDevice)
    {
        for(Device d : this.devices)
        {
            if(d.getFk_DeviceOld() != null) {
                if (d.getFk_DeviceOld().equals(tmpDevice)) return d;
            }
        }
        Device device = getDeviceReplacementFromAPI(tmpDevice);
        if(device != null)
        {
            return device;
        }

        return null;
    }

    @Override
    public boolean getUserInfo()
    {
        final boolean[] success = {false};
        String url = Constants.BASE_URL + Constants.API_PREFIX + Constants.API_PERMISSION;
        try {
            Ion.with(Constants.mContext)
                    .load(HttpMethod.GET, url)
                    .addHeader("Authorization", "JWT " + Constants.JWT)
                    .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                    .asString(Charset.forName("UTF-8"))
                    .withResponse()
                    .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                        {
                            if(result.getHeaders().code() == 200)
                            {
                                try {
                                    JSONArray arr = new JSONArray(result.getResult());
                                    JSONObject jo = arr.getJSONObject(0);
                                    boolean is_staff = jo.getBoolean("is_staff");
                                    boolean is_active = jo.getBoolean("is_active");
                                    String email = jo.getString("email");
                                    int selected_company_id = jo.getInt("selected_company");
                                    JSONArray companies = jo.getJSONArray("companies");
                                    Map<Integer, String> retMap = new Gson().fromJson(
                                            companies.toString(), new TypeToken<HashMap<Integer, String>>() {}.getType()
                                    );

                                    ApiAdapter.this.user = new User(email,selected_company_id,is_staff,is_active, retMap);
                                    success[0] = true;

                                } catch (JSONException jsonException) {
                                    jsonException.printStackTrace();

                                }
                            }
                        }
                    })
                    .get(10, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
            return false;
        }

        return success[0];
    }


    @Override
    public boolean setUserSelectedCompany(int companyId) {
        final boolean[] success = {false};
        String url = Constants.BASE_URL + Constants.SET_COMPANY_ID;
        try {
            Ion.with(Constants.mContext)
                    .load(HttpMethod.POST, url)
                    .addHeader("Authorization", "JWT " + Constants.JWT)
                    .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                    .setBodyParameter("preference_name", "config__selected_company")
                    .setBodyParameter("value", String.valueOf(companyId))
                    .asString(StandardCharsets.UTF_8)
                    .withResponse()
                    .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                        {
                            if(result.getHeaders().code() == 200)
                            {
                                    success[0] = true;
                            }
                        }
                    })
                    .get(10, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
            return false;
        }

        return success[0];
    }

    @Override
    public boolean isUserStaff() {
        if(getUserInfo()){
            return ApiAdapter.this.user.isStaff();
        }
        return false;
    }


    /**
     * Get loaded groups
     * @return groups
     */
    public ArrayList<DeviceGroup> getGroups()
    {
        if(this.groups != null)
        {
            return this.groups;
        }
        return null;
    }

    @Override
    public void postDevice(Device device, MapActivity activity)
    {
        return;
    }


    /**
     * Download Image from url into Bitmap
     * @param url String url
     * @return Bitmap
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws TimeoutException
     */
    public Bitmap loadImage(String url) throws InterruptedException, ExecutionException {
        final Bitmap myBitmap[] = {null};
        Ion.with(Constants.mContext)
                .load(HttpMethod.GET, url)
                .addHeader("Authorization", "JWT " + Constants.JWT)
                .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                .withBitmap()
                .asBitmap()
                .setCallback(new FutureCallback<Bitmap>() {
                    @Override
                    public void onCompleted(Exception e, Bitmap result) {
                        if(e == null)
                        {
                            myBitmap[0] = result;
                        }
                    }
                }).get();

        return myBitmap[0];

    }

    @Override
    public int saveImage(File file, Device device) throws ExecutionException, InterruptedException {
        final int ids[] = {-1};
        String uploadUrl = Constants.BASE_URL + "/upload/";//Constants.IMAGE_UPLOAD;
        Ion.with(Constants.mContext)
                .load(HttpMethod.POST,uploadUrl)
                .addHeader("Authorization", "JWT " + Constants.JWT)
                .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                .uploadProgressHandler(new ProgressCallback()
                    {
                        @Override
                        public void onProgress(long uploaded, long total)
                        {
                            System.out.println("uploaded " + (int)uploaded + " Total: "+total);
                        }
                    })
                .setMultipartFile("file", file)
                .setMultipartParameter("fk_Device", String.valueOf(device.getId()))
                .asString()
                .setCallback(new FutureCallback<String>()
                    {
                        @Override
                        public void onCompleted(Exception e, String result)
                        {
                            if(e == null)
                            {
                                try {
                                    JSONObject json = new JSONObject(result);
                                    int newImageId = json.getInt("id");
                                    ids[0] = newImageId;
                                } catch (JSONException jsonException) {
                                    jsonException.printStackTrace();
                                    ids[0] = -1;
                                }
                            }
                        }
                    })
                .get();
        return ids[0];
    }


    /**
     * Class for getting data from database on background
     * When completed calls method asyncCallback in Constants.ACTIVITY
     */
    private class BackgroundTaskGet extends AsyncTask<Void, Void, Void>
    {

        private LatLng position = null;
        private float radius = -1;
        private String url_devices = Constants.API_DEVICES;
        private boolean authorized = true;

        public BackgroundTaskGet()
        {

        }

        public BackgroundTaskGet(LatLng center, float radius)
        {
            this.position = center;
            this.radius = radius;
        }


        @Override
        protected Void doInBackground(Void... voids)
        {

            url_devices = Constants.API_DEVICES;
            if (this.position != null )
            {
                int radius_int = (int) this.radius;
                int mode = Constants.FILTER_MODE.ordinal();
                url_devices = String.format("%s/?lat=%s&lng=%s&radius=%d&mode=%d", Constants.API_DEVICES, position.latitude, position.longitude, radius_int, mode);
            }
            else
            {
                url_devices = url_devices + "/?mode=" + Constants.FILTER_MODE.ordinal();
            }

            String[] addresses = null;
            if (!Constants.META_DATA_LOADED) {
                addresses = new String[]{Constants.API_COMPANIES, Constants.API_DOMAINS, Constants.API_TYPES,
                        Constants.API_ROLES, Constants.API_MANUFACTURERS, Constants.API_IMAGES, Constants.API_SUPPLIERS,
                        Constants.API_STATUSES, url_devices, Constants.API_EVENTS, Constants.API_GROUPS,
                        Constants.API_CONTRACTS, Constants.API_CONTACTS};

            }
            else{
                addresses = new String[]{url_devices};
            }
            arr = new ArrayList<>();
            try{
            for (int i = 0; i < addresses.length; i++) {
                if(!authorized) break;
                int finalI = i;
                String sufix = "/";
                if(addresses[i].contains("?")) sufix = "";
                String[] finalAddresses = addresses;
                Ion.with(Constants.mContext)
                        .load(HttpMethod.GET, Constants.BASE_URL + addresses[i] + sufix)
                        .addHeader("Authorization", "JWT " + Constants.JWT)
                        .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                        .asString(Charset.forName("UTF-8"))
                        .withResponse()
                        //.setCallback(new FutureCallback<String>()
                        .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>()
                        {
                            @Override
                            public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                            {
                                if(result == null)
                                {
                                    authorized = false;
                                    return;
                                }
                                if (e == null) {
                                if(result.getHeaders().code() == 401)
                                {
                                    authorized = false;
                                }
                                else {
                                    authorized = true;
                                    JSONArray o = null;
                                    try {

                                        o = new JSONArray(new String(result.getResult().getBytes(), "UTF-8"));

                                        assert o != null;
                                        arr.add(new JSONResponse(finalAddresses[finalI], o));


                                    } catch (JSONException | UnsupportedEncodingException jsonException) {
                                        jsonException.printStackTrace();
                                    }
                                }

                                } else {
                                    System.err.println(e);
                                }

                                }
                            }).get();


                }

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if(authorized)
            {
                JSONArray json_companies = null;
                JSONArray json_domains = null;
                JSONArray json_types = null;
                JSONArray json_roles = null;
                JSONArray json_manufacturers = null;
                JSONArray json_images = null;
                JSONArray json_suppliers = null;
                JSONArray json_statuses = null;
                JSONArray json_devices = null;
                JSONArray json_events = null;
                JSONArray json_groups = null;
                JSONArray json_contracts = null;
                JSONArray json_contacts = null;
                for (JSONResponse jr : arr) {
                    switch (jr.url) {
                        case Constants.API_COMPANIES:
                            json_companies = jr.value;
                            break;
                        case Constants.API_DOMAINS:
                            json_domains = jr.value;
                            break;
                        case Constants.API_TYPES:
                            json_types = jr.value;
                            break;
                        case Constants.API_ROLES:
                            json_roles = jr.value;
                            break;
                        case Constants.API_MANUFACTURERS:
                            json_manufacturers = jr.value;
                            break;
                        case Constants.API_IMAGES:
                            json_images = jr.value;
                            break;
                        case Constants.API_SUPPLIERS:
                            json_suppliers = jr.value;
                            break;
                        case Constants.API_STATUSES:
                            json_statuses = jr.value;
                            break;
                        case Constants.API_EVENTS:
                            json_events = jr.value;
                            break;
                        case Constants.API_GROUPS:
                            json_groups = jr.value;
                            break;
                        case Constants.API_CONTRACTS:
                            json_contracts = jr.value;
                            break;
                        case Constants.API_CONTACTS:
                            json_contacts = jr.value;
                            break;
                        //devices url, can be different because lat,lng and radius
                        default:
                            json_devices = jr.value;
                            break;
                    }
                }
                //MUST LOADS OBJECTS IN THIS ORDER TO KEEP INTEGRITY
                try {
                    if(!Constants.META_DATA_LOADED) {
                        serializer = new Serializer(companies, roles, domains, suppliers, images, devices, contracts, statuses, manufacturers, groups, types, events, contacts);
                        companies = serializer.serializeCompanies(json_companies);
                        domains = serializer.serializeDomains(json_domains);
                        types = serializer.serializeTypes(json_types);
                        roles = serializer.serializeRoles(json_roles);
                        manufacturers = serializer.serializeManufacturers(json_manufacturers);
                        images = serializer.serializeImages(json_images);
                        suppliers = serializer.serializeSuppliers(json_suppliers);
                        statuses = serializer.serializeStatuses(json_statuses);
                        devices = serializer.serializeDevices(json_devices);
                        events = serializer.serializeEvents(json_events);
                        groups = serializer.serializeGroups(json_groups);
                        contracts = serializer.serializeContracts(json_contracts);
                        contacts = serializer.serializeContacts(json_contacts);
                        parseOldDevices();
                        Constants.META_DATA_LOADED = true;
                    }
                    else{
                        devices = serializer.serializeDevices(json_devices);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Constants.OFFLINE_DEVICES = new OfflineFileAdapter().getDevicesFromLogFile();
                Constants.ACTIVITY.asyncCallback();
            }
            else
            {
                refreshLogin(this);
            }
        }
    }

    /**
     * Refresh JWT TOKEN and call given task
     * @param task task to call if new JWT token is taken
     */
    private void refreshLogin(AsyncTask task)
    {
        String username = Constants.USER_USERNAME;
        String password = Constants.USER_PASSWORD;
        boolean response = this.authenticate(username,password);
        if(response)
        {
            if(task instanceof BackgroundTask)
            {
                Query q = ((BackgroundTask) task).query;
                JSONObject jo = ((BackgroundTask) task).json;

                new BackgroundTask(q,jo).execute();
            }
            else if(task instanceof BackgroundTaskGet)
            {
                if(((BackgroundTaskGet) task).position != null && ((BackgroundTaskGet) task).radius != -1)
                {
                    LatLng pos = ((BackgroundTaskGet) task).position;
                    float radius = ((BackgroundTaskGet) task).radius;
                    loadAsync(pos,radius);
                }
                else
                {
                    loadAsync();
                }
            }
        }
        else
        {
            showOfflineErrorDialog();
        }
    }

    /**
     * Refresh JWT token
     * @return success
     */
    private boolean refreshLogin()
    {
        String username = Constants.USER_USERNAME;
        String password = Constants.USER_PASSWORD;
        boolean response = this.authenticate(username,password);
        return response;
    }

    /**
     * Show error dialog when no data and let user choose to exit or continue in offline mode
     */
    private void showOfflineErrorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Constants.mContext);
        builder.setMessage(R.string.error_no_data)
                .setNegativeButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        System.exit(0);
                    }
                })
                .setPositiveButton(R.string.contiue_with_offline_mode, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Constants.WORKING_MODE = WORKING_MODES.OFFLINE;
                                Constants.ADAPTER = new OfflineFileAdapter();
                            }
                        }
                );
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    /**
     * Show error dialog with given msg
     * @param msg message
     */
    private void showErrorDialog(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Constants.mContext);
        builder.setMessage(msg)
                .setNegativeButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dismiss
                    }
                });

        // Create the AlertDialog object and return it
        builder.create().show();
    }

    /**
     * Class for response from rest api
     */
    private class JSONResponse
    {
        public String url;
        public JSONArray value;

        public JSONResponse(String url, JSONArray value)
        {
            this.url = url;
            this.value = value;
        }
    }

    private class BackgroundTask extends AsyncTask<Void, Void, Void>
    {
        public final Query query;
        public final JSONObject json;
        private boolean authorized;
        private boolean fromScanner = false;

        public BackgroundTask(Query query, JSONObject object)
        {
            this.query = query;
            this.json = object;
            Constants.RESPONSE_CODE = -1;
            Constants.RESPONSE_MESSAGE = "";
            Constants.IS_ADDING_DEVICE = false;
        }

        public BackgroundTask(Query query, JSONObject object, boolean fromScanner)
        {
            this.query = query;
            this.json = object;
            Constants.RESPONSE_CODE = -1;
            Constants.RESPONSE_MESSAGE = "";
            this.fromScanner = fromScanner;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            String URL = Constants.BASE_URL + this.query.url + "/";
            JsonObject jsonObject = new JsonParser().parse(this.json.toString()).getAsJsonObject();
            try {
                Ion.with(Constants.mContext)
                        .load(this.query.method,URL)
                        .addHeader("Authorization", "JWT " + Constants.JWT)
                        .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                        .setJsonObjectBody(jsonObject)
                        .asString(Charset.forName("UTF-8"))
                        .withResponse()
                        .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>()
                        {
                            @Override
                            public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                            {
                                if(result.getHeaders().code() == 401)
                                {
                                 authorized = false;
                                }
                                else {
                                    authorized = true;

                                    Constants.RESPONSE_CODE = result.getHeaders().code();
                                    Constants.RESPONSE_MESSAGE = result.getResult();
                                }
                            }
                        })
                        .get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if(authorized) {
                if(!fromScanner)
                {
                    Constants.IS_ADDING_DEVICE = true;
                }
                Constants.ACTIVITY.asyncCallback();
            }
            else
            {
                refreshLogin(this);
            }
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public boolean updateObject(String url, JSONObject json)
    {
        try {
            com.koushikdutta.ion.Response<String> response = this.sendRequest(url, json, HttpMethod.PUT);
            if(response == null || response.getHeaders().code() != 200)
            {
                showErrorDialog(response.getResult());
                return false;
            }
            else {
                return true;
            }

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }

    }



    /**
     * Sends json to given url
     * if JWT token expired renew it
     * @param url url of object
     * @param json updated json
     * @param method HttpMethod
     * @return response
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private com.koushikdutta.ion.Response<String> sendRequest(String url, JSONObject json, String method) throws ExecutionException, InterruptedException {
        final com.koushikdutta.ion.Response response[] = {null};
        String jsonstr = json.toString();
        JsonObject jsonObject = new JsonParser().parse(jsonstr).getAsJsonObject();
        Ion.with(Constants.mContext)
                .load("PUT", url)
                .addHeader("Authorization", "JWT " + Constants.JWT)
                .addHeader("Accept-Language", Locale.getDefault().toLanguageTag())
                .addHeader("Content-Type","application/json")
                //.setStringBody(jsonstr)
                .setJsonObjectBody(jsonObject)
                .asString(Charset.forName("UTF-8"))
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result)
                    {
                        if (e == null)
                        {
                            if(result.getHeaders().code() == 401) //UNAUTHORIZED
                            {
                                if(refreshLogin())
                                {
                                    //call itself with renew authorization
                                    try {
                                        sendRequest(url, json, method);
                                    } catch (ExecutionException | InterruptedException executionException) {
                                        executionException.printStackTrace();
                                    }
                                }
                            }
                            else
                            {
                                response[0] = result;
                            }
                        }
                    }
                }).get();

        return response[0];
    }


}
