
package com.giesl.centralregister.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import com.giesl.centralregister.R;
import com.giesl.centralregister.adapters.EndpointAdapter;
import com.giesl.centralregister.adapters.OfflineFileAdapter;
import com.giesl.centralregister.classes.Company;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.DeviceType;
import com.giesl.centralregister.classes.Manufacturer;
import com.giesl.centralregister.classes.OfflineDevice;
import com.giesl.centralregister.classes.Sensor;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.dialogs.CustomDialog;
import com.giesl.centralregister.dialogs.GroupChooserDialog;
import com.giesl.centralregister.interfaces.IActivity;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.giesl.centralregister.scanner.FullScannerActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MapActivity extends AppCompatActivity implements IActivity, OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraMoveCanceledListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private static final String TAG = "MAP ACTIVITY:";
    private static final int MARKER_SIZE = 40;
    private long GPS_UPDATE_INTERVAL = 2 * 1000;  /* 2 secs */
    private long GPS_FASTEST_INTERVAL = 1000; /* 1 sec */
    // Request code for selecting a PDF document.
    private static final int PICK_FILE_RETURN_CODE = 2;
    public static final int READ_EXTERNAL_STORAGE = 112;

    //ICONS SETTINGS
    String ICON_PREFIX = "sharp";
    String ICON_SUFFIX = "24";

    private MapView mapView;
    private GoogleMap gmap;
    private SearchView searchBar;
    private Context mContext = MapActivity.this;
    private FloatingActionButton fabBottom;
    private Constants.MAP_MODES mode;
    private LatLng centerOfCzech = Constants.CENTER_OF_CZECH;
    private GoogleApiClient mGoogleApiClient;
    private LocationManager locationManager;
    private LatLng currentLatLng;
    private LocationRequest mLocationRequest;
    private Location mLocation;
    private Sensor tmpSensor = null;
    private Marker clickedMarker = null;
    private LatLng lastLocation;
    private Typeface typeface;
    private ImageView imageIcon;
    private ProgressBar progressBar;
    private Marker tmp_marker;
    private Device tmp_device;
    private ArrayList<Marker> markers;
    private LatLngBounds lastBounds;
    private float lastDistance;
    private ImageButton btn_filter;
    private CustomDialog filterDialog;
    private Marker customLocationMarker;
    private ImageButton btn_add_admin;
    private ImageButton btn_reload;
    private FloatingActionButton fabImage;
    private boolean loaded;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(Constants.WORKING_MODE == WORKING_MODES.OFFLINE)
        {
            setTheme(R.style.AppThemeOffline);
        }
        setContentView(R.layout.activity_map);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(Constants.GOOGLE_API_KEY);
        }

        this.initGPS();


        mapView = findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        Intent myIntent = getIntent();
        this.isLocationEnabled();
        //this.mode = (Constants.MAP_MODES) myIntent.getExtra("mode",  Constants.MAP_MODES.VIEW);
        this.mode = (Constants.MAP_MODES) myIntent.getSerializableExtra("mode");


        this.initGUI();
        this.getTypeface();
    }



    /**
     * Initialize GPS
     */
    private void initGPS()
    {
        //init google api
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }


    private void getTypeface()
    {
        if(this.typeface == null) {

            this.typeface = ResourcesCompat.getFont(this, R.font.material_icons);
        }
    }


    @Override
    @NotNull
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(Constants.GOOGLE_API_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(Constants.GOOGLE_API_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void initGUI()
    {
        //Disable landscape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Fixed portrait orientation

        //load views into variables
        this.searchBar = findViewById(R.id.searchViewMap);
        this.fabBottom = findViewById(R.id.fabBottom);
        this.fabImage = findViewById(R.id.fab_image);
        this.fabImage.hide();
        //Image view is used for rendering icon, that is used in markers
        this.imageIcon = findViewById(R.id.image_icon);
        this.progressBar = findViewById(R.id.progressbar_loading);

        this.btn_filter = findViewById(R.id.button_filter);
        //WITHOUT LOADED DATA CANNOT APPLY FILTERS
        this.btn_filter.setEnabled(false);
        this.btn_add_admin = findViewById(R.id.button_add_admin);
        this.btn_reload = findViewById(R.id.button_reload);

        this.setMode();

        this.setListeners();


    }

    /**
     * SET MAP BUTTONS BASED ON this.mode
     */
    private void setMode()
    {
        this.tmpSensor = null;
        setFab(this.mode);
        setAdminButton();
        setReloadButton();
    }

    /**
     * Set visibility to reload button based on work mode
     */
    private void setReloadButton()
    {
        //If offline mode change reload button to act as import file
        if(Constants.WORKING_MODE == WORKING_MODES.OFFLINE)
        {
            this.btn_reload.setImageResource(R.drawable.sharp_download_24);
            this.btn_reload.setVisibility(View.VISIBLE);
            this.btn_reload.setEnabled(true);
        }
        else
        {
            this.btn_reload.setEnabled(true);
            this.btn_reload.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Set visibility to admin add button based if user have access to
     */
    private void setAdminButton()
    {
        if(Constants.WORKING_MODE == WORKING_MODES.OFFLINE)
        {
            this.btn_add_admin.setImageResource(R.drawable.sharp_network_check_24);
            this.btn_add_admin.setVisibility(View.VISIBLE);
        }
        else
        {
            if (Constants.user_isStaff) {
                this.btn_add_admin.setVisibility(View.VISIBLE);
            } else {
                this.btn_add_admin.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Set fab icon and visibility based on mode
     * @param mode Current map mode
     */
    private void setFab(Constants.MAP_MODES mode)
    {
        switch(this.mode) {

            case INSTALL:
            case REPLACE:
                this.fabBottom.setImageResource(android.R.drawable.ic_menu_add);
                break;
            case VIEW:
                this.fabBottom.hide();
                break;
        }



    }

    @Override
    public void onBackPressed()
    {
        switch (this.mode)
        {

            case INSTALL:
            case REPLACE:
                finish();
                break;
            case VIEW:
                finish();
                Intent myIntent = new Intent(MapActivity.this, MapActivity.class);
                myIntent.putExtra("mode", Constants.MAP_MODES.VIEW);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MapActivity.this.startActivity(myIntent);
                break;
        }
        super.onBackPressed();
    }

    @Override
    public void setListeners()
    {
        this.searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                String location = searchBar.getQuery().toString();
                List<Address> addressList = null;
                if (location != null | !location.equals("")) {
                    Geocoder geocoder = new Geocoder(mContext);
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);
                            String msg = getString(R.string.location_found) + address.getLocality() + "," + address.getPostalCode();
                            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            LatLng latlng = new LatLng(address.getLatitude(), address.getLongitude());
                            gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 12));
                        } else {
                            Toast.makeText(mContext, R.string.error_search, Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, R.string.error_search, Toast.LENGTH_LONG).show();
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                return false;
            }
        });

        this.fabBottom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                switch (mode)
                {

                    case INSTALL:
                    case REPLACE:
                        addDevice();
                        break;
                    case VIEW:
                        break;
                }
            }
        });

        this.fabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickedMarker != null && !clickedMarker.equals(customLocationMarker)) {
                    Intent intent = new Intent(MapActivity.this, MyCameraActivity.class);
                    Device device = getDeviceFromMarker(clickedMarker);
                    int id = device.getId();
                    String name = device.getName();
                    intent.putExtra("deviceID",id);
                    intent.putExtra("deviceName", name);
                    MapActivity.this.startActivity(intent);
                }
                else
                {
                    Toast.makeText(MapActivity.this,R.string.no_device_choosen, Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.btn_filter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showFilterDialog();
            }
        });


        this.btn_add_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.WORKING_MODE == WORKING_MODES.OFFLINE)
                {
                    if(!EndpointAdapter.isNetworkAvailable())
                    {
                        showNoConnectionDialog();
                    }
                    else {
                        launchLoginActivity();
                    }
                }
                else
                {
                    Intent myIntent = new Intent(MapActivity.this, AdminWebActivity.class);
                    MapActivity.this.startActivity(myIntent);
                }
            }
        });

        this.btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.WORKING_MODE.equals(WORKING_MODES.OFFLINE))
                {
                    Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/iotRegister/");
                    openFile(selectedUri);
                }
                else {
                    if (gmap != null) {
                        loadDevicesToMap(gmap);
                    }
                }
            }
        });

    }

    /**
     * Opens file chooser for text files
     * @param pickerInitialUri starting uri for file chooser
     */
    private void openFile(Uri pickerInitialUri) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");

        // Optionally, specify a URI for the file that should appear in the
        // system file picker when it loads.
        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);

        startActivityForResult(intent, PICK_FILE_RETURN_CODE);
    }


    /**
     * Lanuch login activity for renew login
     */
    private void launchLoginActivity()
    {
        Intent myIntent = new Intent(MapActivity.this, MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Constants.mContext = null;
        MapActivity.this.startActivity(myIntent);
    }

    /**
     * Shows filter dialog
     */
    private void showFilterDialog()
    {

        if(this.filterDialog == null) {
            this.filterDialog = new CustomDialog(MapActivity.this);
        }
        this.filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.filterDialog.show();

    }

    /**
     * Add device after click on add button
     */
    private void addDevice()
    {
        this.fabBottom.setEnabled(false);
        this.progressBar.setVisibility(View.VISIBLE);
        showGroupsChooserDialog(tmp_device);
    }

    /**
     * Shows selector dialog for choose groups for device
     * @param device Selected device
     */
    private void showGroupsChooserDialog(Device device)
    {
        if(Constants.WORKING_MODE == WORKING_MODES.ONLINE) {
            GroupChooserDialog chooserDialog = new GroupChooserDialog(MapActivity.this, device);
            chooserDialog.show();
        }
        //offline mode
        else
        {
            Constants.ADAPTER.postDevice(device,MapActivity.this);
        }

    }

    /**
     * Update existing device
     */
    private void updateDevice(Device device)
    {

        Constants.ADAPTER.putDevice(device, MapActivity.this);

    }

    /**
     * edit existing device
     */
    private void editDevice()
    {
        if(this.clickedMarker != null) {
            Constants.EDIT_ACTIVITY.setHWID(getDeviceFromMarker(this.clickedMarker).getHWID());
            finish();
        }
        else
        {
            Toast.makeText(MapActivity.this,R.string.no_device_choosen,Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Hide keyboard from screen
     */
    private void hideKeyboard()
    {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Pass selected device to next activity
     */
    private void passDevice(Marker marker)
    {

        Device device = getDeviceFromMarker(marker);
        if(device instanceof OfflineDevice) {
            Toast.makeText(MapActivity.this,R.string.only_in_offline_mode_can_edit,Toast.LENGTH_SHORT).show();
        }
        else if (device == null) //Custom location
        {
            Constants.TMP_DEVICE = device;
            Constants.TMP_LOCATION = marker.getPosition();

            Constants.ACTIVITY = MapActivity.this;
            Intent myIntent = new Intent(MapActivity.this, FullScannerActivity.class);
            MapActivity.this.startActivity(myIntent);
        }
        else {

            if( device.getEnabled()) {
                Constants.TMP_DEVICE = device;
                Constants.TMP_LOCATION = marker.getPosition();

                Constants.ACTIVITY = MapActivity.this;
                Intent myIntent = new Intent(MapActivity.this, FullScannerActivity.class);
                MapActivity.this.startActivity(myIntent);
            }
            else
            {
                showErrorDialog(getString(R.string.error_cannot_edit), getString(R.string.cannot_edit));
            }
        }
    }

    /**
     * Get device instance from Marker
     * @param marker Marker
     * @return Device
     */
    private Device getDeviceFromMarker(Marker marker)
    {
        Device device = (Device) marker.getTag();
        return device;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == PICK_FILE_RETURN_CODE
                && resultCode == Activity.RESULT_OK) {
            // The result data contains a URI for the document or directory that
            // the user selected.
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                ((OfflineFileAdapter) Constants.ADAPTER).importDataFromUri(uri);

            }
        }
    }

    @Override
    public void onCameraIdle()
    {

        if(this.currentLatLng == null)
        {
            return;
        }
        if(this.lastBounds == null)
        {
            Constants.mContext = MapActivity.this;
            loadDevicesToMap(gmap);
        }

        LatLngBounds bounds = gmap.getProjection().getVisibleRegion().latLngBounds;
        if(!this.lastBounds.contains(bounds.getCenter()) || distance(bounds.getCenter(),bounds.northeast) > (this.lastDistance/2))
        {
            //this prevents for multiple loading in offline mode
            if(Constants.WORKING_MODE.equals(WORKING_MODES.OFFLINE) && loaded)
            {
                return;
            }
            loadDevicesToMap(gmap);
            loaded = true;
        }



    }

    @Override
    public void onCameraMoveCanceled()
    {

    }

    @Override
    public void onCameraMove()
    {
        hideKeyboard();
        //if (this.mode == Constants.MODE_ADD || this.mode == Constants.MODE_ADJUST || this.mode == Constants.MODE_REPLACE) {
        if (this.mode == Constants.MAP_MODES.INSTALL || this.mode == Constants.MAP_MODES.REPLACE) {
            this.tmp_marker.setPosition(gmap.getCameraPosition().target);
            if (!this.tmp_marker.isInfoWindowShown()) {
                this.tmp_marker.showInfoWindow();
            }
            this.tmp_marker.setVisible(true);
            //TODO - fix problem when bitmap is not showing in some unspecified conditions

            if(Constants.WORKING_MODE == WORKING_MODES.ONLINE) {
                Bitmap bitmap = getBitmap(tmp_device, Color.GRAY);
                if (bitmap != null) {
                    this.tmp_marker.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmap(tmp_device, Color.GRAY)));
                }
            }


            this.tmp_device.setLocation(new LatLng(gmap.getCameraPosition().target.latitude, gmap.getCameraPosition().target.longitude));
        }


    }

    @Override
    public void onCameraMoveStarted(int reason)
    {

        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {

        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {

        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        gmap = googleMap;
        gmap.setOnCameraIdleListener(this);
        gmap.setOnCameraMoveStartedListener(this);
        gmap.setOnCameraMoveListener(this);
        gmap.setOnCameraMoveCanceledListener(this);
        gmap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        gmap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(mContext);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(mContext);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(mContext);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        gmap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(Marker marker) {
                try {
                    Device device = (Device) marker.getTag();
                    String hwid = device.getHWID();
                    int start = hwid.indexOf("mid=");
                    start += "mid=".length();
                    String mid = hwid.substring(start, start+8);
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("MID", mid);
                    clipboard.setPrimaryClip(clip);
                    String msg = getString(R.string.mid_copy_msg,(String) mid);
                    Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(MapActivity.this,R.string.error_permission,Toast.LENGTH_LONG).show();
            return;
        }

        //if location isn't enabled move to center of czech republic
        if(this.isLocationEnabled())
        {
            gmap.setMyLocationEnabled(true);
        }
        else {
            gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerOfCzech, 6.0f));
        }
        switch (this.mode)
        {

            case INSTALL:
                LatLng location = gmap.getCameraPosition().target;
                this.tmp_device= Constants.TMP_DEVICE;
                this.tmp_device.setLocation(location);
                this.tmp_marker = addDeviceToMap(gmap, this.tmp_device,true);
                tmp_marker.setPosition(location);
                break;
            case REPLACE:
                this.tmp_device= Constants.TMP_DEVICE;
                LatLng location_new = gmap.getCameraPosition().target;
                this.tmp_device.setLocation(location_new);
                this.tmp_marker = addDeviceToMap(gmap, this.tmp_device,true);
                tmp_marker.setPosition(location_new);
                break;
            case VIEW:
                gmap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        clickedMarker = marker;
                        if (!marker.equals(customLocationMarker)) {
                            fabImage.show();
                        }
                        return false;
                    }
                });

                gmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
                {
                    @Override
                    public void onInfoWindowClick(Marker marker)
                    {
                        if(marker == customLocationMarker)
                        {
                            showAddToCustomLocationDialog(marker.getPosition());
                        }
                        else {
                            passDevice(marker);
                        }
                    }
                });
                gmap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener()
                {
                    @Override
                    public void onMapLongClick(LatLng latLng)
                    {
                        if(customLocationMarker == null) {
                            customLocationMarker = gmap.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .title(getString(R.string.custom_marker))
                                    .snippet(getString(R.string.custom_marker))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        }
                        else
                        {
                            customLocationMarker.setPosition(latLng);
                        }



                        //showAddToCustomLocationDialog(latLng);
                    }
                });
                gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
                {
                    @Override
                    public void onMapClick(LatLng latLng)
                    {
                        fabImage.hide();
                        if(customLocationMarker != null)
                        {
                            customLocationMarker.remove();
                            customLocationMarker = null;
                        }
                    }
                });
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    /**
     * GPS LOCATIONS IMPLEMENTED METHODS
     */

    /**
     *IF GPS position is changed
     * @param location
     */
    @Override
    public void onLocationChanged(Location location)
    {


        // You can now create a LatLng Object for use with maps

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if(currentLatLng == null) {
            switch (this.mode) {

                case INSTALL:
                    if (currentLatLng == null)
                        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19.0f));
                    break;
                case REPLACE:
                    if (tmp_device != null)
                        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(Constants.TMP_LOCATION, 19.0f));
                        if(this.tmp_marker != null)
                        {
                            tmp_marker.setPosition(latLng);
                        }
                        Constants.TMP_LOCATION = null;
                    break;
                case VIEW:
                    gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19.0f));
                    break;
            }
        }
        this.currentLatLng = latLng;

        
    }



    /**
     * @param source Source location
     * @param destination  Destination location
     * @return float distance in meters bettwen two points
     */
    public float distance ( LatLng source, LatLng destination)
    {
        float lat_a, lng_a, lat_b, lng_b;
        lat_a = (float) source.latitude;
        lng_a = (float) source.longitude;
        lat_b = (float) destination.latitude;
        lng_b = (float) destination.longitude;
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b-lat_a);
        double lngDiff = Math.toRadians(lng_b-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    }

    /**
     * Starts gps location refreshing
     */
    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(GPS_UPDATE_INTERVAL)
                .setFastestInterval(GPS_FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(MapActivity.this, getString(R.string.text_missing_location), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i)
    {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        loadDevicesToMap(gmap);
        new AlertDialog.Builder(this.mContext)
                .setTitle(R.string.GPS_ERROR_TITLE)
                .setMessage(R.string.GPS_ERROR_MESSAGE)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Checks if location is enabled
     * @return is enabled
     */
    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }



    /********************************************NEW SECTION FOR REGISTER REST API*****************************/

    /**
     * Loads all devices to gmap
     * @param gmap Google map
     * @return if success
     */
    private boolean loadDevicesToMap(GoogleMap gmap)
    {
        if(Constants.WORKING_MODE.equals(WORKING_MODES.ONLINE)) {
            this.btn_reload.setEnabled(false);
        }
        this.progressBar.setVisibility(View.VISIBLE);
        Constants.ACTIVITY = MapActivity.this;
        this.lastLocation = gmap.getCameraPosition().target;
        LatLngBounds bounds = gmap.getProjection().getVisibleRegion().latLngBounds;
        LatLng northeast = bounds.northeast;
        LatLng southwest = bounds.southwest;
        float distance = (float) (distance(northeast,southwest) * Constants.RADIUS_MULTIPLIER);
        this.lastDistance = distance;
        this.lastBounds = bounds;


        Constants.ADAPTER.loadAsync(lastLocation, distance / 2);


        return true;
    }

    /**
     * Callback for async task, that loads data on background
     */
    @Override
    public void asyncCallback()
    {
        if(Constants.WORKING_MODE == WORKING_MODES.ONLINE)
        {
            this.btn_reload.setEnabled(true);
            this.btn_filter.setEnabled(true);
            if(Constants.OFFLINE_DEVICES != null)
            {
                for(Device device : Constants.OFFLINE_DEVICES)
                {
                    addDeviceToMap(gmap, device);
                }
            }
        }
        //LOADING DATA
            if (!Constants.IS_ADDING_DEVICE)
            {
                EndpointAdapter adapter = Constants.ADAPTER;
                ArrayList<? extends Device> devices = adapter.getDevices();
                this.clearMap();
                this.markers = new ArrayList<>();
                //ready for clustering
                if (devices != null) {
                    for (Device d : devices) {
                        if (this.tmp_device != null) {
                            if (!tmp_device.equals(d)) {
                                Marker marker = addDeviceToMap(gmap, d);
                                markers.add(marker);
                            } else {
                                markers.add(addDeviceToMap(gmap, d, true));
                            }

                        } else {
                            markers.add(addDeviceToMap(gmap, d));
                        }
                    }
                    applyFilter();
                    String msg = getString(R.string.loaded_devices, devices.size());
                    Toast.makeText(MapActivity.this, msg, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MapActivity.this, R.string.no_devices, Toast.LENGTH_SHORT).show();
                }
                this.progressBar.setVisibility(View.INVISIBLE);
            }
            //is adding new device
            else {
                Constants.IS_ADDING_DEVICE = false;
                this.fabBottom.setEnabled(true);
                this.progressBar.setVisibility(View.INVISIBLE);
                if (this.mode == Constants.MAP_MODES.INSTALL || this.mode == Constants.MAP_MODES.REPLACE) {
                    if (Constants.RESPONSE_CODE < 200 || Constants.RESPONSE_CODE > 300) {
                        showAddingErrorDialog();
                    } else {
                        ShowAddingOKDialog();
                    }
                }
            }

    }

    /**
     * Show dialog when device is successfully uploaded
     */
    private void ShowAddingOKDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
        builder.setTitle(R.string.device_add_success)
                .setMessage(R.string.device_add_message)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeActivity();

                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();

    }

    /**
     * Close Map activity and lauch new Map activity with cleared activity stack
     */
    private void closeActivity()
    {
        Intent myIntent = new Intent(MapActivity.this, MapActivity.class);
        myIntent.putExtra("mode", Constants.MAP_MODES.VIEW);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        MapActivity.this.startActivity(myIntent);
    }

    /**
     * Show error dialog with given parameters
     * @param title Dialog title
     * @param message Dialog message
     */
    private void showErrorDialog(String title, String message)
    {
        new AlertDialog.Builder(MapActivity.this)
                .setTitle(title)
                .setMessage(message)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Show dialog when adding device error
     */
    private void showAddingErrorDialog()
    {

        showErrorDialog(getString(R.string.device_add_error), getString(R.string.error_add_device,Constants.RESPONSE_MESSAGE));
    }

    /**
     * Show dialog for confirmation if user want to add new device to custom location
     * @param latLng location of click
     */
    private void showAddToCustomLocationDialog(LatLng latLng)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
        builder.setTitle(R.string.custom_marker)
                .setMessage(R.string.custom_marker_msg)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(customLocationMarker != null) {
                            customLocationMarker.remove();
                        }
                        Constants.TMP_DEVICE = null;
                        Constants.TMP_LOCATION = latLng;
                        Constants.ACTIVITY = MapActivity.this;
                        Intent myIntent = new Intent(MapActivity.this, FullScannerActivity.class);
                        MapActivity.this.startActivity(myIntent);
                    }
                })
                .setNegativeButton(R.string.back, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(customLocationMarker != null) {
                            customLocationMarker.remove();
                        }
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    /**
     * Remove loaded markers from map
     */
    private void clearMap()
    {
        if(this.markers != null)
        {
            for (Marker m : this.markers) {
                m.remove();
            }
        }
    }

    /**
     * Add device to google map
     * @param gmap map
     * @param device Device
     * @return Marker
     */
    private Marker addDeviceToMap(GoogleMap gmap, Device device)
    {
        if (device == null || gmap == null){
            return null;
        }
        MarkerOptions markerOptions;
        if(Constants.WORKING_MODE == WORKING_MODES.ONLINE && !(device instanceof OfflineDevice)) {
            Bitmap bitmap = getBitmap(device);
            markerOptions = new MarkerOptions()
                    .position(device.getLocation())
                    .title(device.getMarkerTitleString())
                    .snippet(device.getSnippetString());
            if (bitmap != null) {
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
            }
        }
        //offline mode
        else
        {
            float color = BitmapDescriptorFactory.HUE_ORANGE;
            Date installed = device.getDateInstalled();
            if(installed != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(installed);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                color =(float) (dayOfMonth * month);
            }
            markerOptions = new MarkerOptions()
            .position(device.getLocation())
            .title(device.getMarkerTitleString())
            .snippet(device.getSnippetString())
            .icon(BitmapDescriptorFactory.defaultMarker(color));

        }

            Marker marker = gmap.addMarker(markerOptions);
            marker.setTag(device);
            return marker;


    }

    /**
     * Add device to google map
     * @param gmap map
     * @param device Device
     * @param gray if marker color is gray
     * @return Marker
     */
    private Marker addDeviceToMap(GoogleMap gmap, Device device, boolean gray)
    {
        MarkerOptions markerOptions;
        Bitmap bitmap;
        if (gray) {
            bitmap = getBitmap(device, Color.LTGRAY);
        } else {
            bitmap = getBitmap(device);
        }
        markerOptions = new MarkerOptions()
                    .position(device.getLocation())
                    .title(device.getName())
                    .snippet(device.getComment());


        if(bitmap != null)
        {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
        }

        return gmap.addMarker(markerOptions);

    }

    /**
     * Get device icon
     * @param d Device
     *          If bitmap is not present in included font or error occurred -> return null
     * @return Bitmap - icon
     */
    private Bitmap getBitmap(Device d)
    {

        ImageView ivVectorImage = this.imageIcon;
        try {
            String iconName = d.getTypes().get(0).getIcon();
            String resourceName = String.format("%s_%s_%s",ICON_PREFIX, iconName, ICON_SUFFIX);
            this.setImageResourceByName(ivVectorImage, resourceName);
        }
        catch (Exception e)
        {
            System.err.println("Error with loading drawable");
            //Toast.makeText(MapActivity.this,R.string.error_bad_drawable,Toast.LENGTH_SHORT).show();
            return null;
        }
        ivVectorImage.setColorFilter(d.getColor());

        Drawable yourDrawable = imageIcon.getDrawable();
        if (yourDrawable != null) {
            int width = yourDrawable.getIntrinsicWidth();
            int height = yourDrawable.getIntrinsicHeight();

            Canvas canvas = new Canvas();
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            canvas.setBitmap(bitmap);
            yourDrawable.draw(canvas);
            final Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
            if (emptyBitmap.equals(bitmap)) {
                Toast.makeText(MapActivity.this, R.string.error_bad_drawable, Toast.LENGTH_SHORT).show();
                return null;
            }
            return bitmap;
        }
        else {
            return null;
        }
    }


    /**
     * Get device icon with given color
     * @param d Device
     * @param color Color for icon
     * @return Bitmap icon
     */
    private Bitmap getBitmap(Device d, int color)
    {
        ImageView ivVectorImage = this.imageIcon;
        try {
            String iconName = d.getTypes().get(0).getIcon();
            String resourceName = String.format("%s_%s_%s",ICON_PREFIX, iconName, ICON_SUFFIX);
            this.setImageResourceByName(ivVectorImage, resourceName);
        }
        catch (Exception e)
        {
            System.err.println("Error with loading drawable");
            return null;
        }
        ivVectorImage.setColorFilter(color);

        Drawable yourDrawable = imageIcon.getDrawable();
        int width = yourDrawable.getIntrinsicWidth();
        int height = yourDrawable.getIntrinsicHeight();

        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        yourDrawable.draw(canvas);

        return  bitmap;
    }


    /**
     * Set new icon for drawable
     * @param view
     * @param drawableName
     */
    private void setImageResourceByName(ImageView view, String drawableName)
    {
        Context context = view.getContext();
        int id = context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName());
        view.setImageResource(id);
    }

    /**
     * Applies new filter on map with given params
      * @param devicesIds
     * @param companies
     * @param manufacturers
     * @param types
     * @param newRequest
     */
    public void applyFilters(ArrayList<Integer> devicesIds, ArrayList<Company> companies, ArrayList<Manufacturer> manufacturers, ArrayList<DeviceType> types, boolean newRequest)
    {
        Filter filter = new Filter();
        Constants.MAP_FILTER = filter;
        filter.companies = companies;
        filter.devicesIds = devicesIds;
        filter.manufacturers = manufacturers;
        filter.types = types;
        if (newRequest)
        {
            this.loadDevicesToMap(gmap);
            return;
        }

        if(!filter.isEmpty()) {
            applyFilter();
        }
        else
        {
            cleanFilter();
        }
    }

    public class Filter
    {
        public ArrayList<Integer> devicesIds;
        public ArrayList<Company> companies;
        public ArrayList<Manufacturer> manufacturers;
        public ArrayList<DeviceType> types;


        public boolean contains(Device device)
        {
            int id = device.getId();
            if (devicesIds.contains(id)) return true;
            if (companies.contains(device.getFk_Manufacturer())) return true;
            if (manufacturers.contains(device.getFk_Manufacturer())) return true;
            for(DeviceType dt : device.getTypes())
                {
                    if(types.contains(dt)) return true;
                }
            return false;
        }


        public boolean isEmpty()
        {
            //return false;
            return (this.devicesIds.isEmpty() && this.types.isEmpty() && this.manufacturers.isEmpty() && this.companies.isEmpty());
        }
    }

    /**
     * Apply filter on already loaded markers in map
     */
    private void applyFilter()
    {
        Filter filter = Constants.MAP_FILTER;
        if(filter != null && !filter.isEmpty()) {
            for (Marker m : this.markers) {

                Device d = getDeviceFromMarker(m);
                m.setVisible(filter.contains(d));
            }
        }
    }

    /**
     * remove all filters and show all markers
     */
    private void cleanFilter()
    {

        for(Marker m : this.markers)
        {
            m.setVisible(true);
        }
        Constants.MAP_FILTER  = null;

    }

    /**
     * Saved passed device
     * @param device device
     */
    public void saveDevice(Device device)
    {
        if(device.getId() != 0)
        {
            updateDevice(device);
        }
        //create whole new device
        else
        {
            Constants.ADAPTER.postDevice(device,MapActivity.this);
        }
    }


    /**
     * Show error dialog when network is not available
     */
    private void showNoConnectionDialog()
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MapActivity.this);
        builder.setMessage(R.string.error_no_connection_message)
                .setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                ;
        // Create the AlertDialog object and return it
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}


