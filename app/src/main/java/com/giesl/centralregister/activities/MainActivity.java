package com.giesl.centralregister.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.giesl.centralregister.R;
import com.giesl.centralregister.adapters.ApiAdapter;
import com.giesl.centralregister.adapters.EndpointAdapter;
import com.giesl.centralregister.adapters.OfflineFileAdapter;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.WORKING_MODES;

import java.io.File;

public class MainActivity extends AppCompatActivity
{

    private Button btnLogin;
    private static final int REQUEST = 112;
    private EditText textUsername;
    private EditText textPassword;
    private RadioButton btn_production;
    private RadioButton btn_development;
    private EditText text_server_custom;
    private RadioButton btn_custom;
    private ImageButton btn_expand_servers;
    private TextView text_server;
    private ProgressBar progressBar;
    private Button btn_open_file;
    private Button btn_offline_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Constants.mContext = MainActivity.this;

        this.btn_development = findViewById(R.id.radio_development);
        this.btn_production = findViewById(R.id.radio_production);
        this.btn_custom = findViewById(R.id.radio_custom_server);
        this.btn_open_file = findViewById(R.id.button_open_offline_log);
        this.text_server_custom = findViewById(R.id.text_server_custom);
        this.progressBar = findViewById(R.id.progressbar_loading);


        initGUI();
        this.getPermissions();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
        
        this.writeTest();

    }

    private void writeTest()
    {

        checkExternalMedia();




    }

    private void checkExternalMedia(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
    }

    /**
     * loads GUI elements to variables;
     */
    private void initGUI()
    {
        this.btnLogin = (Button) findViewById(R.id.btn_login);
        this.btn_expand_servers = findViewById(R.id.button_show_servers);
        this.textUsername = (EditText) findViewById(R.id.input_username);
        this.textPassword = (EditText) findViewById(R.id.input_password);
        this.text_server = findViewById(R.id.text_server);
        this.btn_offline_mode = findViewById(R.id.button_offline_mode);

        if(!EndpointAdapter.isNetworkAvailable())
        {
            showNoConnectionDialog();
        }
        else {
            Constants.WORKING_MODE = WORKING_MODES.ONLINE;
            Constants.ADAPTER = new ApiAdapter();
        }

        // if exists offline log file show button
        if(is_offline_log_file_exists())
        {
            this.btn_open_file.setVisibility(View.VISIBLE);
        }
        else
        {
            this.btn_open_file.setVisibility(View.INVISIBLE);
        }


        this.setListeners();
        this.text_server.setText(getString(R.string.selected_server, getString(R.string.server_production)));

    }

    /**
     * Set listeners for GUI elements
     */
    private void setListeners()
    {

        this.btnLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onClickLoginBtn();
            }
        });


        this.btn_custom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked) {
                    text_server_custom.setVisibility(View.VISIBLE);
                }
                else
                {
                    text_server_custom.setVisibility(View.GONE);
                }
            }
        });

        this.btn_expand_servers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                RadioGroup group = findViewById(R.id.radio_group_servers);
                group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId)
                    {
                        RadioButton radiobtn = findViewById(checkedId);
                        text_server.setText(getString(R.string.selected_server, radiobtn.getText()));

                    }
                });
                if(group.getVisibility() == View.GONE)
                {
                    group.setVisibility(View.VISIBLE);
                }
                else
                {
                    group.setVisibility(View.GONE);
                }

            }
        });

        this.btn_open_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogFile();
            }
        });

        this.btn_offline_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchOfflineMode();
            }
        });
    }

    /**
     * Opens file with offline logs
     */
    private void openLogFile() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/iotRegister/");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "resource/folder");

        if (intent.resolveActivityInfo(getPackageManager(), 0) != null)
        {
            startActivity(intent);
        }
        else
        {
            // if you reach this place, it means there is no any file
            // explorer app installed on your device
            showNoExplorerOnDeviceAlert();
        }
    }



    private void onClickLoginBtn()
    {
        this.progressBar.setVisibility(View.VISIBLE);
        this.btnLogin.setEnabled(false);
        if(this.btn_production.isChecked()) {
            Constants.BASE_URL = Constants.BASE_URL_PRODUCTION;
        }
        else if(this.btn_development.isChecked())
        {
            Constants.BASE_URL = Constants.BASE_URL_DEVELOPMENT;
        }
        else if(this.btn_custom.isChecked())
        {
            if(this.text_server_custom.getText().toString().equals("")) {
                Toast.makeText(MainActivity.this, R.string.no_input, Toast.LENGTH_SHORT).show();
            }
            else {
                Constants.BASE_URL = this.text_server_custom.getText().toString();
            }
        }
        if(EndpointAdapter.isNetworkAvailable()) {



            if (checkLogin()) {
                Constants.USER_USERNAME = this.textUsername.getText().toString();
                Constants.USER_PASSWORD = this.textPassword.getText().toString();
                Constants.mContext = MainActivity.this;
                Constants.user_isStaff = Constants.ADAPTER.isUserStaff();
                 launchCompanyChooserActivity();
            } else {
                this.progressBar.setVisibility(View.INVISIBLE);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.bad_login_message)
                        .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                btnLogin.setEnabled(true);
                            }
                        })
                        .setNegativeButton(R.string.contiue_with_offline_mode, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                launchOfflineMode();

                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();
                btnLogin.setEnabled(true);
            }
        }
        else {
            showNoConnectionDialog();
        }
    }

    /**
     * Launch map activity with offline mode
     */
    private void launchOfflineMode()
    {
        Constants.WORKING_MODE = WORKING_MODES.OFFLINE;
        Constants.ADAPTER = new OfflineFileAdapter();
        getTheme().applyStyle(R.style.AppThemeOffline, true);
        launchMapActivity();
    }

    /**
     * Check given credentials with web authenticator and get set jwt token
     * @return if credentials are valid
     */
    private boolean checkLogin()
    {

        return(Constants.ADAPTER.authenticate(textUsername.getText().toString(),textPassword.getText().toString()));
        //return(Constants.ADAPTER.Authenticate("admin","admin"));

    }

    private void launchCompanyChooserActivity()
    {
        Intent myIntent = new Intent(MainActivity.this, CompanyChooserActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Constants.mContext = null;
        MainActivity.this.startActivity(myIntent);
    }

    private void launchMapActivity()
    {
        Intent myIntent = new Intent(MainActivity.this, MapActivity.class);
        myIntent.putExtra("mode", Constants.MAP_MODES.VIEW);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Constants.mContext = null;
        MainActivity.this.startActivity(myIntent);
    }


    private void getPermissions()
    {
        //ask for permissions
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET, android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (!hasPermissions(MainActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, REQUEST);
            }
        }
    }

    /**
     * Checks permission for ceratin context
     * @param context context
     * @param permissions list of permissions
     * @return boolean
     */
    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Show error dialog when network is not available
     */
    private void showNoConnectionDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.error_no_connection_message)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        launchOfflineMode();
                    }
                })
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.exit(0);
                    }}
                );
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows error dialog when no file explorer is installed od device
     */
    private void showNoExplorerOnDeviceAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.error_no_explorer)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Checks if offline log file exists
     * @return
     */
    private boolean is_offline_log_file_exists()
    {
        File root = android.os.Environment.getExternalStorageDirectory();
        String path = root.getAbsolutePath() + "/" + Constants.LOG_FILE_DIR + "/" + Constants.LOG_FILE_OFFLINE;
        File file = new File(path);
        return file.exists();

    }

}
