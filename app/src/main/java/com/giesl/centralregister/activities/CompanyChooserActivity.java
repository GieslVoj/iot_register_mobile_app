package com.giesl.centralregister.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.giesl.centralregister.R;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.IActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CompanyChooserActivity extends AppCompatActivity implements IActivity {

    private Button btnApply;
    private Spinner spinner;
    private TextView label_loading;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_chooser);
        Constants.mContext = CompanyChooserActivity.this;
        Constants.ACTIVITY = CompanyChooserActivity.this;
        initGUI();
        setListeners();

    }

    @Override
    public void initGUI() {
        this.spinner = findViewById(R.id.spinner_companies);
        this.btnApply = findViewById(R.id.btn_next);
        this.progressBar = findViewById(R.id.progress_bar);
        this.label_loading = findViewById(R.id.tv_loading);

        loadItemsToSpinner();
        loadMetaData();
    }

    /**
     * Loads companies to spinner
     */
    private void loadItemsToSpinner() {

        List<String> companies = new ArrayList<String>(Constants.ADAPTER.user.getCompanies().values());
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, companies);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    public static <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    private void launchMapActivity() {
        Intent myIntent = new Intent(CompanyChooserActivity.this, MapActivity.class);
        myIntent.putExtra("mode", Constants.MAP_MODES.VIEW);
        Constants.mContext = null;
        CompanyChooserActivity.this.startActivity(myIntent);
    }

    @Override
    public void setListeners() {
        this.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setEnabled(false);
                btnApply.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                int id = getKey(Constants.ADAPTER.user.getCompanies(), spinner.getSelectedItem().toString());

                if (Constants.ADAPTER.setUserSelectedCompany(id)) {
                    Constants.ADAPTER.getUserInfo();
                    launchMapActivity();
                } else {
                    new AlertDialog.Builder(Constants.mContext)
                            .setTitle(R.string.error_no_connection)
                            .setMessage(R.string.error_no_connection_message)

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }


                spinner.setEnabled(true);
                btnApply.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });


    }

    @Override
    public void asyncCallback() {
        this.btnApply.setEnabled(true);
        this.label_loading.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.GONE);
    }

    public void loadMetaData() {
        if(Constants.ADAPTER != null){
            this.btnApply.setEnabled(false);
            this.progressBar.setVisibility(View.VISIBLE);
            this.label_loading.setVisibility(View.VISIBLE);
            Constants.ADAPTER.loadAsync(Constants.DEFAULT_LOCATION, 0);
        }
    }

}