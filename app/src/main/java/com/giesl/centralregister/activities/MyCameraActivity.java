package com.giesl.centralregister.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.giesl.centralregister.R;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.Image;
import com.giesl.centralregister.classes.OfflineDevice;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.github.chrisbanes.photoview.PhotoView;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MyCameraActivity extends Activity {

    // TODO - make no limit for images
    // TODO - fix replace

    private static final int CAMERA_REQUEST = 1888;
    private static final int GALLERY_REQUEST = 1889;
    private static final int FILE_TAG = R.id.File_Tag;
    private ImageView imageView0, imageView1, imageView2, imageView3;
    private FrameLayout mainFrame;
    private ImageView views[];
    private String imagesUrl[]; //get images url or path to file
    private boolean changed[]; //tracks changed images
    private boolean delete[]; //tracks deleted images
    private int selectedPanel = -1;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private int defaultImageResource = R.drawable.sharp_add_photo_alternate_24;
    private Uri imageUri;
    private Device device;
    private PhotoView photoView;
    private ProgressBar progressBar;
    private boolean isOnline = Constants.WORKING_MODE == WORKING_MODES.ONLINE;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.mContext = MyCameraActivity.this;
        setContentView(R.layout.camera_activity);
        // load device from intent
        Intent myIntent = this.getIntent();
        int id = myIntent.getIntExtra("deviceID", -1);
        String name = myIntent.getStringExtra("deviceName");
        if (id == -1) {
            showErrorDialog("No device");
        }
        this.device = Constants.ADAPTER.getDevice(id);
        if(this.device == null)
        {
            this.device = Constants.ADAPTER.getDevice(name);
        }


        this.progressBar = findViewById(R.id.progressBar);
        this.progressBar.setVisibility(View.VISIBLE);

        this.photoView = findViewById(R.id.photo_view);
        this.mainFrame = findViewById(R.id.frameLayout);

        this.imageView0 = findViewById(R.id.imageView);
        this.imageView1 = (ImageView) this.findViewById(R.id.imageView1);
        this.imageView2 = findViewById(R.id.imageView2);
        this.imageView3 = findViewById(R.id.imageView3);
        this.views = new ImageView[4];
        views[0] = imageView0;
        views[1] = imageView1;
        views[2] = imageView2;
        views[3] = imageView3;
        setViewsImage();
        setViewsListeners(views);
        this.progressBar.setVisibility(View.INVISIBLE);

        this.imagesUrl = new String[views.length];
        this.changed = new boolean[views.length];
        this.delete = new boolean[views.length];

        Button button_save = findViewById(R.id.button_save);
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });




    }

    private int loadImages(int i)
    {
        if (this.device != null && !(this.device instanceof OfflineDevice)) {

            for (Image image : this.device.getImages()) {
                if (i > views.length) break;
                //new ImageDownloader(image.getUrl(),views[i], MyCameraActivity.this).execute();
                Bitmap bitmap = null;
                try {
                    bitmap = Constants.ADAPTER.loadImage(image.getGetUrl());
                    if (bitmap != null) {
                        views[i].setImageBitmap(bitmap);
                        views[i].setTag(image.getId());
                    } else {
                        views[i].setImageDrawable(getDrawable(R.drawable.sharp_error_24));
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    views[i].setImageDrawable(getDrawable(R.drawable.sharp_error_24));
                }
                i++;
            }
        }
        else
        {
            for(File f : ((OfflineDevice) this.device).getOfflineImages())
            {
                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                if (bitmap != null) {
                    views[i].setImageBitmap(bitmap);
                    views[i].setTag(FILE_TAG, f);
                    views[i].setTag(1);
                } else {
                    views[i].setImageDrawable(getDrawable(R.drawable.sharp_error_24));
                }
                i++;
            }
        }
        return i;
    }

    /**
     * Set views image
     * TODO - add existing images in offline mode
     */
    private void setViewsImage() {
        int i = 0;

             i = loadImages(i);

            for (; i < views.length; i++) {
                views[i].setImageDrawable(getDrawable(defaultImageResource));
                views[i].setTag(defaultImageResource);
            }


    }

    /**
     * Set listeners for all imageViews
     *
     * @param views views
     */
    private void setViewsListeners(ImageView[] views) {
        for (int i = 0; i < views.length; i++) {
            int finalI = i;
            views[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPanel = finalI;
                    showDialog();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.error_camera_permission, Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, R.string.error_camera_permission, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {
                Bitmap thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                views[selectedPanel].setImageBitmap(thumbnail);
                views[selectedPanel].setTag(0);
                String imageurl = getRealPathFromURI(imageUri);
                this.imagesUrl[selectedPanel] = imageurl;
                this.changed[selectedPanel] = true;
            } catch (Exception e) {
                e.printStackTrace();
            }


            selectedPanel = -1;
        }
        if (requestCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    views[selectedPanel].setImageBitmap(BitmapFactory.decodeFile(picturePath));
                    views[selectedPanel].setTag(0);
                    this.imagesUrl[selectedPanel] = picturePath;
                    changed[selectedPanel] = true;
                    cursor.close();
                }
            }

        }

    }

    /**
     * Opens camera
     */
    public void openCamera() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAMERA_REQUEST);

        }
    }

    /**
     * Save images
     */
    private void save()
    {

        ProgressBar bar = findViewById(R.id.progressBar);
        bar.setVisibility(View.VISIBLE);
        this.mainFrame.setVisibility(View.INVISIBLE);

        for(int i = 0;i<views.length;i++)
        {
            if(delete[i])
            {
                deleteImage(views[i]);
            }
            else if(changed[i])
            {
                postImage(views[i]);
            }

        }
        bar.setVisibility(View.INVISIBLE);
        launchMapActivity();

    }

    /**
     * Launches map activity with cleared stack
     */
    private void launchMapActivity()
    {
        Intent myIntent = new Intent(MyCameraActivity.this, MapActivity.class);
        myIntent.putExtra("mode", Constants.MAP_MODES.VIEW);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Constants.mContext = null;
        MyCameraActivity.this.startActivity(myIntent);
    }

    /**
     * Remove foreign key to image
     * @param view selected view
     */
    private void deleteImage(ImageView view){
        if((Integer) view.getTag() != 0) {
            if (this.isOnline) {
                try {
                    int id = (Integer) view.getTag();
                    Image image = Constants.ADAPTER.getImage(id);
                    if (image != null) {
                        Constants.mContext = MyCameraActivity.this;
                        removeDeviceFromImage(image);
                    }
                } catch (JSONException e) {
                    showErrorDialog(getString(R.string.error_delete));
                }
            }
            //OFFLINE MODE
            else
            {
                File f = (File) view.getTag(FILE_TAG);
                if(!f.delete())
                {
                    showErrorDialog(getString(R.string.error_delete));
                }

            }
        }

    }

    /**
     * Remove foreign key from Image object
     * @param image
     */
    private void removeDeviceFromImage(Image image) throws JSONException {
        String imageUrl = image.getUrl();
        JSONObject json = new JSONObject();
        json.put("fk_Device", JSONObject.NULL);
        if(!Constants.ADAPTER.updateObject(imageUrl, json))
        {
            showErrorDialog(getString(R.string.error_delete));
        }



    }

    /**
     * Create tmp file and post it to API
     * @param view selected view
     */
    private void postImage(ImageView view)
    {
        try {
            File outputDir = MyCameraActivity.this.getCacheDir(); // context being the Activity pointer
            File outputFile = File.createTempFile("tmp_image", ".jpg", outputDir);
            Bitmap bm=((BitmapDrawable)view.getDrawable()).getBitmap();
            FileOutputStream out = new FileOutputStream(outputFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored

            Constants.ADAPTER.saveImage(outputFile, device);
        } catch (ExecutionException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show dialog for actions with selected photo
     */
    private void showDialog() {
        String camera = getString(R.string.take_photo);
        String gallery = getString(R.string.choose_from_gallery);
        String delete = getString(R.string.delete);
        String cancel = getString(R.string.cancel_button);
        String show = getString(R.string.show);

        final CharSequence[] options = {show, camera, gallery, delete, cancel};

        AlertDialog.Builder builder = new AlertDialog.Builder(MyCameraActivity.this);
        builder.setTitle(R.string.choose_picture);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals(camera)) {
                    openCamera();

                } else if (options[item].equals(gallery)) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, GALLERY_REQUEST);

                } else if (options[item].equals(cancel)) {
                    dialog.dismiss();
                } else if (options[item].equals(delete)) {
                    MyCameraActivity.this.delete[selectedPanel] = true;
                    views[selectedPanel].setImageDrawable(getDrawable(defaultImageResource));
                } else if (options[item].equals(show)) {
                    ImageView selected = views[selectedPanel];
                    Drawable drawable = selected.getDrawable();
                    if ((Integer) selected.getTag() == defaultImageResource) {
                        Toast.makeText(MyCameraActivity.this, R.string.no_image, Toast.LENGTH_SHORT).show();
                    } else {
                        showPicture(drawable);
                    }
                }
            }
        });
        builder.show();
    }

    /**
     * Show selected picture to fullscreen view
     *
     * @param drawable drawable to show
     */
    private void showPicture(Drawable drawable) {

        if(drawable != null) {
            this.mainFrame.setVisibility(View.INVISIBLE);
            this.photoView.setImageDrawable(drawable);
            this.photoView.setVisibility(View.VISIBLE);
        }
        else
        {
            Toast.makeText(MyCameraActivity.this,R.string.no_image,Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * Show AlertDialog with given message
     *
     * @param msg message to show
     */
    public void showErrorDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MyCameraActivity.this);
        builder.setMessage(msg)
                .setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        onBackPressed();
                    }
                });
        // Create the AlertDialog object and show it
        builder.create().show();


    }

    @Override
    public void onBackPressed() {
        if (this.photoView.getVisibility() == View.VISIBLE) {
            this.photoView.setVisibility(View.INVISIBLE);
            this.mainFrame.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


}
