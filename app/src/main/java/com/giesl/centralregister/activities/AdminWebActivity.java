package com.giesl.centralregister.activities;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.giesl.centralregister.R;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.IActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.Map;

public class AdminWebActivity extends AppCompatActivity implements IActivity {

    private Button btn_device, btn_group, btn_type, btn_event;
    private WebView webView;
    private FloatingActionButton fabFinish;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_web);
        initGUI();
    }

    @Override
    public void initGUI() {


        this.webView = findViewById(R.id.webview);
        this.fabFinish = findViewById(R.id.fabFinish);
        initWebView();
        setListeners();

    }

    @Override
    public void setListeners() {

        this.fabFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void asyncCallback() {

    }

    @Override
    public void onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack();
        } else {
            finish();
        }
    }

    private void initWebView()
    {
        this.webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(AdminWebActivity.this, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }


            public void onPageFinished(WebView view, String url) {
                //TODO - change to JWT token
                if(url.contains("/login")) {
                    String user = Constants.USER_USERNAME;
                    String pwd = Constants.USER_PASSWORD;
                    webView.loadUrl("javascript:(function() { document.getElementById('id_username').value = '" + user + "'; ;})()");
                    webView.loadUrl("javascript:(function() { document.getElementById('id_password').value = '" + pwd + "'; ;})()");
                }
            }

        });
        WebSettings webSettings = this.webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        this.webView.loadUrl(Constants.REGISTER_FIELDS_URL);
    }


}
