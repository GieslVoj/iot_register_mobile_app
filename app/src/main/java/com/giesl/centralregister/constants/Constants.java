package com.giesl.centralregister.constants;

import android.content.Context;
import android.net.Uri;

import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.adapters.ApiAdapter;
import com.giesl.centralregister.adapters.EndpointAdapter;
import com.giesl.centralregister.classes.DatabaseConnection;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.classes.OfflineDevice;
import com.giesl.centralregister.interfaces.IActivity;
import com.giesl.centralregister.interfaces.IDeviceList;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public final class Constants
{



    // always return itself
    private static final Constants ourInstance = new Constants();
    public static Context mContext;



    /**
     * Always return itself
     * @return Constrants
     */
    public static Constants getInstance()
    {
        return ourInstance;
    }


    //variables for transfer between activities
    public static String SCANNED_ID = null;
    public static IDeviceList GETIDACTIVITY = null;
    public static boolean IS_DEVICE_LOADED;
    public static Device TMP_DEVICE;
    public static LatLng TMP_LOCATION;
    public static IActivity ACTIVITY;
    public static boolean IS_ADDING_DEVICE = false;
    public static int RESPONSE_CODE;
    public static String RESPONSE_MESSAGE;
    public static IDeviceList EDIT_ACTIVITY;
    public enum SHOW_MODE {ACTUAL, ONLY_REPLACED, ALL}
    public static SHOW_MODE FILTER_MODE = SHOW_MODE.ACTUAL;
    public static ArrayList<OfflineDevice> OFFLINE_DEVICES;
    public static final String LOG_FILE = "logs.txt";
    public static final String LOG_FILE_OFFLINE = "logs_offline.txt";
    public static final String LOG_FILE_DIR = "iotRegister";
    public static final String OFFLINE_IMAGES_DIR = "images";
    public static boolean user_isStaff = false;
    public static MapActivity.Filter MAP_FILTER;
    public static short COUNTER_BAD_SCAN = 0;
    public static boolean META_DATA_LOADED = false;


    //remembering last selected items
    public static int LAST_GROUP_FOR_LOOKUP = 0;
    public static int LAST_STATUS_SELECTED = 0;
    public static int LAST_COMPANY_SELECTED = 0;
    public static int LAST_MANUFACTURER_SELECTED = 0;


    //BASE VARIABLES
    //SERVERS
    public static final String BASE_URL_DEVELOPMENT = "http://192.168.43.164:8000";
    public static final String BASE_URL_PRODUCTION = "https://iot-register.prod.app.citiq.cloud";
    public static String BASE_URL = BASE_URL_PRODUCTION;
    public static final String REGISTER_FIELDS_URL = BASE_URL + "/admin/register/";

    public static final String GOOGLE_API_KEY = "AIzaSyBc0ubir5XNrhIGi-BYRCwSsWt_YZf3KWg";
    public static final LatLng CENTER_OF_CZECH = new LatLng(49.8001306,14.3539658);
    public static final LatLng DEFAULT_LOCATION = new LatLng(49.07446215057577, 17.431388485197584);
    public static EndpointAdapter ADAPTER = new ApiAdapter();

    // TODO
    public static final String INSTRUCTION_WEB = "http://www.citiq.cz";

    //MAP
    public static enum MAP_MODES {INSTALL, REPLACE,VIEW}
    public static final Short MODE_ADD  = 0;
    public static final Short MODE_EDIT = 1;
    public static final Short MODE_VIEW = 2;
    public static final float RADIUS_MULTIPLIER = 2;

    //WORKING MODE
    public static String WORKING_MODE = WORKING_MODES.OFFLINE;

    // AUTHORIZATION VARIABLES
    public static String JWT;

    //API VARIABLES
    public static final String API_NULL = "null";


    //USER VARIABLES
    public static String USER_USERNAME;
    public static String USER_PASSWORD;

    // API ADDRESSES
    public static final String API_PREFIX       = "/api";
    public static final String API_DEVICES      = API_PREFIX + "/devices";
    public static final String API_TYPES        = API_PREFIX + "/types";
    public static final String API_EVENTS       = API_PREFIX + "/events";
    public static final String API_IMAGES       = API_PREFIX + "/images";
    public static final String API_CONTRACTS    = API_PREFIX + "/contracts";
    public static final String API_COMPANIES    = API_PREFIX + "/companies";
    public static final String API_SUPPLIERS    = API_PREFIX + "/suppliers";
    public static final String API_STATUSES     = API_PREFIX + "/statuses";
    public static final String API_MANUFACTURERS = API_PREFIX + "/manufacturers";
    public static final String API_DOMAINS      = API_PREFIX + "/domains";
    public static final String API_GROUPS       = API_PREFIX + "/groups";
    public static final String API_ROLES        = API_PREFIX + "/roles";
    public static final String API_CONTACTS     = API_PREFIX + "/contacts";
    public static final String API_JWT          = "/api-token-auth/";
    public static final String API_PERMISSION   = "/user-info/";
    public static final String IMAGE_UPLOAD     = "/image-upload/";
    public static final String SET_COMPANY_ID   = "/ajax-set/";


    /************************************** POSTGREST API ******************************************/


    //Fields
    public static final String RESTAPIADDRESS = "http://81.200.53.78:3000";
    public static final String API_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiaW52ZW50b3J5X3VzZXIifQ.8Gls6IqRrFJeFWaj9JkgZSU79ghwT2GSt6LlHzFNLpc";


    //DATABASE TABLES VALUES
    //TABLE DEVICE
    public static final String TABLE_DEVICE         = "/Device?";
    public static final String COL_DEVICE_ID        = "idDevice";
    public static final String COL_DEVICE_NAME      = "name";
    public static final String COL_DEVICE_LAT       = "lat";
    public static final String COL_DEVICE_LNG       = "lng";
    public static final String COL_DEVICE_CREATED   = "dateAdded";
    public static final String COL_DEVICE_ENABLED   = "enabled";
    public static final String COL_DEVICE_COMMENT   = "comment";
    public static final String COL_DEVICE_CONTRACTID = "idContract_Contract";
    public static final String COL_DEVICE_TYPEID     = "idDeviceType_DeviceType";

    //TABLE DEVICE TYPE
    public static final String TABLE_TYPE            = "/DeviceType?";
    public static final String COL_TYPE_ID           = "idDeviceType";
    public static final String COL_TYPE_NAME         = "name";
    public static final String COL_TYPE_ICON         = "icon";
    public static final String COL_TYPE_DOMAIN       = "domain";
    public static final String COL_TYPE_COLOR        = "color";

    //TABLE COMPANY
    public static final String TABLE_COMPANY        = "Company";
    public static final String COL_COMPANY_ID       = "idCompany";
    public static final String COL_COMPANY_NAME     = "name";
    public static final String COL_COMPANY_LOGIN    = "login";
    public static final String COL_COMPANY_PASSWORD = "password";
    public static final String COL_COMPANY_EMAIL    = "email";
    public static final String COL_COMPANY_CREATED  = "created";

    //TABLE CONTRACT
    public static final String TABLE_CONTRACT       = "Contract";
    public static final String COL_CONTRACT_ID      = "idContract";
    public static final String COL_CONTRACT_NAME    = "name";
    public static final String COL_CONTRACT_TOKEN   = "token";
    public static final String COL_CONTRACT_CREATED = "created";
    public static final String COL_CONTRACT_EXPIRE  = "expiration";
    public static final String COL_CONTRACT_COMMENT = "comment";
    public static final String COL_CONTRACT_COMPLETED = "completed";
    public static final String COL_CONTRACT_COMPANYID = "id_Company_Company";
    public static final String COL_CONTRACT_EMAIL     = "email";

    //TABLE SUPPLIER
    public static final String TABLE_SUPPLIER       = "Supplier";
    public static final String COL_SUPPLIER_ID      = "idSupplier";
    public static final String COL_SUPPLIER_NAME    = "name";

    //TABLE HAS_CONTRACT
    public static final String TABLE_HAS_CONTRACT   = "has_Contract";
    public static final String COL_HAS_CONTRACT_SUPPLIERID      = "idSupplier_Supplier";
    public static final String COL_HAS_CONTRACT_CONTRACTID      = "idContract_Contract";

    //INTENT NAMES
    public static final String INTENT_NAME          = "sensor_name";
    public static final String INTENT_CONTRACTID    = "sensor_contractid";
    public static final String INTENT_TYPEID        = "sensor_typeid";
    public static final String INTENT_LATITUDE      = "sensor_latitude";
    public static final String INTENT_LONGITUDE     = "sensor_longtitude";
    public static final String INTENT_COMMENT       = "sensor_comment";


    //DatabaseConnection instance
    public static DatabaseConnection database = null;



    //Methods
}
