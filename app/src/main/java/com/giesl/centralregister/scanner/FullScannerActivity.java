package com.giesl.centralregister.scanner;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.giesl.centralregister.R;
import com.giesl.centralregister.activities.MapActivity;
import com.giesl.centralregister.adapters.OfflineFileAdapter;
import com.giesl.centralregister.classes.Device;
import com.giesl.centralregister.constants.Constants;
import com.giesl.centralregister.interfaces.IActivity;
import com.giesl.centralregister.interfaces.WORKING_MODES;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class FullScannerActivity extends BaseScannerActivity implements MessageDialogFragment.MessageDialogListener,
        ZXingScannerView.ResultHandler, FormatSelectorDialogFragment.FormatSelectorDialogListener,
        CameraSelectorDialogFragment.CameraSelectorDialogListener, IActivity
{
    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private static final String TAG = "SCANNER_ACTIVITY";
    private ZXingScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;
    private ProgressBar progressbar;
    private String scannedId;
    private Device scannedDevice; //device with scanned
    private Device selectedDevice; //selected device from map
    private Device finalDevice;
    private FloatingActionButton fabBottom;

    private enum actions {install, replace}
    private actions action;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        Constants.SCANNED_ID = null;
        if(state != null) {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = state.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = state.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }

        setContentView(R.layout.activity_simple_scanner);
        //setupToolbar();

        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        setupFormats();
        contentFrame.addView(mScannerView);
        this.progressbar = findViewById(R.id.progressbar);
        this.progressbar.setVisibility(View.INVISIBLE);
        this.fabBottom = findViewById(R.id.fabInsertID);
        this.fabBottom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showInputDialog();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem;

        if(mFlash) {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_off);
        }
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);


        if(mAutoFocus) {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_off);
        }
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        menuItem = menu.add(Menu.NONE, R.id.menu_formats, 0, R.string.formats);
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        menuItem = menu.add(Menu.NONE, R.id.menu_camera_selector, 0, R.string.select_camera);
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.menu_flash:
                mFlash = !mFlash;
                if(mFlash) {
                    item.setTitle(R.string.flash_on);
                } else {
                    item.setTitle(R.string.flash_off);
                }
                mScannerView.setFlash(mFlash);
                return true;
            case R.id.menu_auto_focus:
                mAutoFocus = !mAutoFocus;
                if(mAutoFocus) {
                    item.setTitle(R.string.auto_focus_on);
                } else {
                    item.setTitle(R.string.auto_focus_off);
                }
                mScannerView.setAutoFocus(mAutoFocus);
                return true;
            case R.id.menu_formats:
                DialogFragment fragment = FormatSelectorDialogFragment.newInstance(this, mSelectedIndices);
                fragment.show(getSupportFragmentManager(), "format_selector");
                return true;
            case R.id.menu_camera_selector:
                mScannerView.stopCamera();
                DialogFragment cFragment = CameraSelectorDialogFragment.newInstance(this, mCameraId);
                cFragment.show(getSupportFragmentManager(), "camera_selector");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {}
        String msg = getString(R.string.scan_result);
        this.scannedId = rawResult.getText();
        Constants.SCANNED_ID = rawResult.getText();
        showMessageDialog(msg +" = " + rawResult.getText());


    }

    public void showMessageDialog(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(FullScannerActivity.this);
        builder.setTitle(R.string.scanned);
        builder.setMessage(message)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeActivity();

                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    private void closeActivity()
    {
        this.scannedDevice = Constants.ADAPTER.getDeviceByHWID(this.scannedId);
        this.selectedDevice = Constants.TMP_DEVICE;
            if (scannedDevice == null) {
                Constants.COUNTER_BAD_SCAN++;
                showNoDeviceWithGivenIdDialog();
            } else {
                Constants.COUNTER_BAD_SCAN = 0;
                //Install prepared device on another location
                if (this.selectedDevice == null && Constants.TMP_LOCATION != null) {
                    //this.scannedDevice.setLocation(Constants.TMP_LOCATION);
                    startMap(this.scannedDevice, Constants.MAP_MODES.REPLACE);
                }
                //Install prepared device
                else if (this.scannedDevice.equals(this.selectedDevice)) {
                    startMap(this.scannedDevice, Constants.MAP_MODES.INSTALL);
                }
                //replace
                else if (this.scannedDevice.getFk_DeviceOld() != null && this.scannedDevice.getFk_DeviceOld().equals(this.selectedDevice)) {
                    this.scannedDevice.setLocation(this.selectedDevice.getLocation());
                    Constants.TMP_LOCATION = selectedDevice.getLocation();
                    this.startMap(scannedDevice, Constants.MAP_MODES.REPLACE);
                } else {
                    Device replacementForSelectedDevice = Constants.ADAPTER.getReplacementForDevice(this.selectedDevice);
                    //Install device to another location
                    if (replacementForSelectedDevice == null) {
                        showInstallConflictDialog(this.selectedDevice);
                    }
                    //have replacement
                    else {
                        showReplaceConflictDialog(replacementForSelectedDevice, this.selectedDevice, this.scannedDevice);
                    }

                }
            }




    }

    public void closeMessageDialog() {
        closeDialog("scan_results");
    }

    public void closeFormatsDialog() {
        closeDialog("format_selector");
    }

    public void closeDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if(fragment != null) {
            fragment.dismiss();
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // Resume the camera
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onFormatsSaved(ArrayList<Integer> selectedIndices) {
        mSelectedIndices = selectedIndices;
        setupFormats();
    }

    @Override
    public void onCameraSelected(int cameraId) {
        mCameraId = cameraId;
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    public void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if(mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for(int i = 0; i < ZXingScannerView.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for(int index : mSelectedIndices) {
            formats.add(ZXingScannerView.ALL_FORMATS.get(index));
        }
        if(mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        closeMessageDialog();
        closeFormatsDialog();
    }

    @Override
    public void initGUI()
    {

    }

    @Override
    public void setListeners()
    {

    }

    @Override
    public void asyncCallback()
    {
        Constants.ACTIVITY = null;
        Constants.mContext = null;
        progressbar.setVisibility(View.INVISIBLE);
        switch (this.action) {
            case install:
                if (checkReponse()) {
                    startMap(this.scannedDevice, Constants.MAP_MODES.REPLACE);
                } else {
                    showResponseErrorDialog();
                }
                break;
            case replace:
               if(checkReponse())
               {
                   if(this.finalDevice != null)
                   {
                       startMap(this.finalDevice, Constants.MAP_MODES.REPLACE);
                   }
                   else
                   {
                       showResponseErrorDialog();
                   }
               }
               else
               {
                   showResponseErrorDialog();
               }

                break;
            default:
                throw new IllegalStateException("Unexpected value: " + this.action);
        }
        this.action = null;
    }



    //Checks if response is 2xx
    private boolean checkReponse()
    {
        return Constants.RESPONSE_CODE >= 200 && Constants.RESPONSE_CODE < 300;
    }

    private void startMap(Device device, Constants.MAP_MODES mode)
    {
        Constants.TMP_DEVICE = device;
        Intent mapIntent = new Intent(FullScannerActivity.this,MapActivity.class);
        mapIntent.putExtra("mode", mode);
        FullScannerActivity.this.startActivity(mapIntent);

    }


    /******************************************DIALOGS******************************************/
    /**
     * Show alert dialog with message when scanned id not have related device
     * call finish, when click on positive button
     * options:
     *          back to map activity
     */
    private void showNoDeviceWithGivenIdDialog()
    {
        androidx.appcompat.app.AlertDialog dialog = new androidx.appcompat.app.AlertDialog.Builder(FullScannerActivity.this)
                .setTitle(R.string.no_device_with_id_title)
                .setMessage(R.string.no_device_with_id_message)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();
        if(Constants.COUNTER_BAD_SCAN >= 2)
        {
            dialog.setButton(AlertDialog.BUTTON_NEGATIVE,getString(R.string.contiue_with_offline_mode), new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Constants.WORKING_MODE = WORKING_MODES.OFFLINE;
                    Constants.ADAPTER = new OfflineFileAdapter();
                    scannedDevice = Constants.ADAPTER.getDeviceByHWID(scannedId);
                    if(scannedDevice != null)
                    {
                        if (selectedDevice == null && Constants.TMP_LOCATION != null) {
                            //this.scannedDevice.setLocation(Constants.TMP_LOCATION);
                            startMap(scannedDevice, Constants.MAP_MODES.REPLACE);
                        }
                        else {
                            startMap(scannedDevice, Constants.MAP_MODES.INSTALL);
                        }
                    }
                    else
                    {
                        showMessageDialog("Error");
                    }
                }
            });
        }

        dialog.show();
    }

    /**
     * Show alert dialog when is conflict between scanned and selected device
     * options:
     *          install scanned
     *          back to map activity
     */
    private void showInstallConflictDialog(Device replacedDevice)
    {
        new androidx.appcompat.app.AlertDialog.Builder(FullScannerActivity.this)
                .setTitle(R.string.error_install_confilct_title)
                .setMessage(getString(R.string.error_install_confilct_message,replacedDevice.getHWID()))
                .setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        finish();
                    }
                })
                .setPositiveButton(R.string.install_confilict_proceed, new DialogInterface.OnClickListener()
                {


                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //Move scanned to new location
                        scannedDevice.setLocation(replacedDevice.getLocation());
                        //move old device to default location and wait for response from server
                        replacedDevice.setLocation(Constants.DEFAULT_LOCATION);
                        progressbar.setVisibility(View.VISIBLE);
                        Constants.ACTIVITY = FullScannerActivity.this;
                        action = actions.install;
                        Constants.ADAPTER.putDevice(replacedDevice,FullScannerActivity.this);

                    }
                })
                .setNeutralButton(R.string.replace_with_scanned, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        scannedDevice.setLocation(selectedDevice.getLocation());
                        scannedDevice.setFk_DeviceOld(selectedDevice);
                        Constants.TMP_LOCATION = selectedDevice.getLocation();
                        startMap(scannedDevice, Constants.MAP_MODES.REPLACE);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Show alert dialog when conflict in replacing device
     * options:
     *          replace with scanned
     *          replace with prepared
     *          back to map activity
     */
    private void showReplaceConflictDialog(Device original_replace, Device selectedDevice, Device scannedDevice)
    {
        new androidx.appcompat.app.AlertDialog.Builder(FullScannerActivity.this)
                .setTitle(R.string.error_replace_conflict_title)
                .setMessage(getString(R.string.error_replace_conflict_message,original_replace.getHWID()))
                .setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        finish();
                    }
                })
                .setPositiveButton(R.string.replace_with_scanned, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        action = actions.replace;
                        scannedDevice.setLocation(selectedDevice.getLocation());
                        original_replace.setLocation(Constants.DEFAULT_LOCATION);
                        finalDevice = scannedDevice;
                        Constants.ACTIVITY = FullScannerActivity.this;
                        Constants.ADAPTER.putDevice(original_replace,FullScannerActivity.this);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    /**
     * Show error dialog when response from db is no 2xx
     * options:
     *          finish
     */
    private void showResponseErrorDialog()
    {
        new androidx.appcompat.app.AlertDialog.Builder(FullScannerActivity.this)
                .setTitle(R.string.title_error)
                .setMessage(R.string.error_no_connection_message)
                .setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        finish();
                    }
                })
                .setPositiveButton(R.string.install_confilict_proceed, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Show input dialog for manual insert hwid
     */
    private void showInputDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.manual_hwid_input_title);

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scannedId = input.getText().toString();
                Constants.SCANNED_ID = input.getText().toString();
                String msg = getString(R.string.scan_result);
                showMessageDialog(msg +" = " + input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


}
